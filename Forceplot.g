set terminal postscript eps color enhanced "Helvetica,16" size 15cm,8cm
set output "forceExport.eps"

set xlabel "{/Helvetica=18 Force}"
set ylabel "{/Helvetica=18 Extension}"

set key at 9.9,0.95


f(x) = sinh(x)/(2.0+cosh(x))
p "NewExtensionTest.dat" u 1:2 title "L=200 SAW" lc 0 lt 1 pt 1 ps 2 lw 2.1,\
"nonSAWextension.dat" u 1:2 title "L=200" lc 1 lt 2 pt 2 ps 2 lw 2.1,\
f(x) title "Theory" lc 3 lt 1 lw 3

