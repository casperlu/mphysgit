mean = 2.4733
stDev = 2.1988
gaussian(x) = A*exp(-(x-mean)*(x-mean)/(2*stDev*stDev)) + B
fit gaussian(x) "./histHighestPWMAff.out" u 1:2 via A,B
plot "./histHighestPWMAff.out" u 1:2 with boxes
replot gaussian(x)
