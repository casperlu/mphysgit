mean = 2.6566191803001646
stDev = 2.0684253437652016
gaussian(x) = A*exp(-(x-mean)*(x-mean)/(2*stDev*stDev)) + B
fit gaussian(x) "./2MbpHist.out" u 1:2 via A,B
plot "./2MbpHist.out" u 1:2 with boxes
replot gaussian(x)
