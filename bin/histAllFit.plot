mean = -2.7388601257770127
stDev = 2.2447601097130034
gaussian(x) = A*exp(-(x-mean)*(x-mean)/(2*stDev*stDev)) + B
fit gaussian(x) "./histAllPWMAff.out" u 1:2 via A,B
plot "./histAllPWMAff.out" u 1:2 with boxes
replot gaussian(x)
