set terminal postscript eps color enhanced "Helvetica,16" size 15cm,8cm
set output "forceExport.eps"

set xlabel "{/Helvetica=18 Force}"
set ylabel "{/Helvetica=18 Extension}"

set key at 9.9,0.95


f(x) = sinh(x)/(2.0+cosh(x))
p "extension3.dat" u 1:2 title "L=200" lc 0 lt 1 pt 1 ps 2,\
f(x) title "Theory"

