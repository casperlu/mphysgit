binwidth=1000
bin(x,width)=width*floor(x/width)+width/2.0
set boxwidth binwidth
set table "hist.temp"
plot "HistogramAff6.0Seeds2000.dat" u (bin($1,binwidth)):(1.0) smooth freq with boxes
unset table
fit(x) = exp(-A*x**B)
fit [500:5000] fit(x) "hist6.0.dat" u 1:2 via A,B
plot "hist6.0.dat", fit(x)