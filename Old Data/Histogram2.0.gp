binwidth=5000
bin(x,width)=width*floor(x/width)+width/2.0
set boxwidth binwidth
plot "HistogramAff2.0Seeds1000.dat" u (bin($1,binwidth)):(1.0) smooth freq with boxes
replot 138*exp(-(x**2)/(2*(30000**2)))