set terminal postscript eps color enhanced "Helvetica,16" size 15cm,12cm
set output "affinityProfileExport.eps"

set multiplot layout 2,1
set title "A"
set xlabel "{/Helvetica=18 DNA position}"
set ylabel "{/Helvetica=18 Affinity}"
cut = 12

A=cut
B=35
D=A+B
C=0
E=1000
F=500
eps=10
eps2=0.5
eps3=1

set ytics 0, 5, A
set ytics add (gprintf("%.0f", 15+B ) 15)
set ytics add (gprintf("%.0f", 20+B ) 20)
#set ytics add (gprintf("%.0f", 25+B ) 25)


set arrow 1 from C, A to C, A+eps3 nohead lc rgb "#ffffff" lw 3 front
set arrow 2 from E, A to E, A+eps3 nohead lc rgb "#ffffff" lw 3 front
set arrow 3 from C-eps,A-eps2 to C+eps,A+eps2 nohead front
set arrow 4 from C-eps,A-eps2+eps3 to C+eps,A+eps2+eps3 nohead front
set arrow 5 from E-eps,A-eps2 to E+eps,A+eps2 nohead front
set arrow 6 from E-eps,A-eps2+eps3 to E+eps,A+eps2+eps3 nohead front


set arrow 7 from F, A to F, A+eps3 nohead lc rgb "#ffffff" lw 3 front
set arrow 8 from F-eps,A-eps2 to F+eps,A+eps2 nohead front
set arrow 9 from F-eps,A-eps2+eps3 to F+eps,A+eps2+eps3 nohead front

set yrange [0:20]
set key at 996,19.2

p "AffinityManagerOutput.dat" u 1:($2<cut?$2:cut) w l title "{/Symbol e}_{ns}=0" lc 1 lt 1 lw 2.1,\
"AffinityManagerOutput.dat" u 1:($2>cut+B?$2-B:$2) w l notitle lc 1 lt 1 lw 2.1

#"nonSAWextension.dat" u 1:($2<20?$2:) title "L=200" lc 1 lt 2 pt 2 ps 2 lw 2.1,\

set title "B"
set xlabel "{/Helvetica=18 DNA position}"
set ylabel "{/Helvetica=18 Affinity}"
cut = 12

A=cut
B=35
D=A+B
C=0
E=1000
F=500
eps=10
eps2=0.5
eps3=1

set ytics 0, 5, A
set ytics add (gprintf("%.0f", 15+B ) 15)
set ytics add (gprintf("%.0f", 20+B ) 20)
#set ytics add (gprintf("%.0f", 25+B ) 25)


set arrow 1 from C, A to C, A+eps3 nohead lc rgb "#ffffff" lw 3 front
set arrow 2 from E, A to E, A+eps3 nohead lc rgb "#ffffff" lw 3 front
set arrow 3 from C-eps,A-eps2 to C+eps,A+eps2 nohead front
set arrow 4 from C-eps,A-eps2+eps3 to C+eps,A+eps2+eps3 nohead front
set arrow 5 from E-eps,A-eps2 to E+eps,A+eps2 nohead front
set arrow 6 from E-eps,A-eps2+eps3 to E+eps,A+eps2+eps3 nohead front


set arrow 7 from F, A to F, A+eps3 nohead lc rgb "#ffffff" lw 3 front
set arrow 8 from F-eps,A-eps2 to F+eps,A+eps2 nohead front
set arrow 9 from F-eps,A-eps2+eps3 to F+eps,A+eps2+eps3 nohead front

set yrange [0:20]
set key at 996,19.2

p "AffinityManagerOutputDNAAff4.dat" u 1:($2<cut?$2:cut) w l title "{/Symbol e}_{ns}=4" lc 1 lt 1 lw 2.1,\
"AffinityManagerOutputDNAAff4.dat" u 1:($2>cut+B?$2-B:$2) w l notitle lc 1 lt 1 lw 2.1

#"nonSAWextension.dat" u 1:($2<20?$2:) title "L=200" lc 1 lt 2 pt 2 ps 2 lw 2.1,\
