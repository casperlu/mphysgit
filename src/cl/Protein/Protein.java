package cl.Protein;

import java.util.Random;

import cl.Configurations.Configuration;
import cl.Configurations.EndConfiguration;
import cl.DNA.Grid;
import cl.DNA.PolymerOneProtein;
import cl.Lattice.Cell;
import cl.Lattice.Map;
import cl.ProtConfigurations.JumpOffEnd;
import cl.ProtConfigurations.JumpOffInternal;

public class Protein {

	//Internal variables
	protected Cell pos;
	protected int box, onDNA, N;
	protected PolymerOneProtein poly = null;
	protected Grid grid;
	protected Random rndm;
	protected boolean targetFound;
	protected double affinity, probStayOnInternal, probStayOnEnd, target;
	protected Configuration conf = null;

	/*
	 * These arrays holds all the maps for the protein moves.
	 * They are constant values.
	 */
	protected final Map[] endMaps = JumpOffEnd.getEndMaps();
	protected final JumpOffEnd[] endConfs = JumpOffEnd.getConfigurations();
	protected final Map[][] internalMaps = JumpOffInternal.getInternalMaps();
	protected final JumpOffInternal[] internalConfs = JumpOffInternal.getConfigurations();


	/*
	 * Constructor computes probabilities
	 */
	public Protein(int boxSize, PolymerOneProtein p, Random r, double aff, double target) {
		rndm = r;
		box = boxSize;
		pos = new Cell(box, rndm);
		poly = p;
		N = p.getDNALength();
		grid = poly.grid;
		targetFound = false;
		this.target = target;		//Target value

		setAffinity(aff);
	}

	/*
	 * Second constructor taking specific initial position
	 */
	public Protein(int boxSize, Cell position, PolymerOneProtein p, Random r, double aff, double target) {
		rndm = r;
		box = boxSize;
		pos = position;
		poly = p;
		N = p.getDNALength();
		grid = poly.grid;
		targetFound = false;
		this.target = target;		//Target value

		setAffinity(aff);
	}

	public void update() {

		//3D diffusion
		if (grid.getCell(pos) == -1) {
			update3dDiffusion();
		}

		//Possible 1D diffusion
		else {

			//The protein is now on the DNA
			onDNA = 1;

			//Get the index of the monomer it is attached to
			int index = grid.getCell(pos);

			//Check if monomer is the target
			if (poly.getTarget(index) == target) { targetFound = true; }


			//Only if protein is not at target, should it continue searching
			if (targetFound == false) {

				//Notify polymer that protein is no longer on monomer[index]
				poly.updateOccupation(index, -1);

				//If protein is attached to an end monomer
				if (index == 0 || index == (poly.getPoly().length-1)) {
					updateEndProtein(index);
				}

				//If protein is attached to an internal monomer
				else {	updateInternalProtein(index); }
			}
		}
	}

	private void update3dDiffusion() {
		//Do 3d random walk
		onDNA = 0;
		conf = EndConfiguration.ENDMONOMER;		//Protein is same as for endmonomer
		Map[] relativeMoves = conf.getPossibleMoves();

		/*
		 * Probability of either move in configuration is
		 * 1/6. Choose one at random. Add the relative move to the
		 * position of nearest to get the new move of the end
		 * monomer. Try the move: If cell is empty, perform move.
		 */
		int random = (int) (rndm.nextDouble()*6);
		Cell newMove = Cell.Add(relativeMoves[random], pos);
		pos = newMove;

		//In case newMove is on polymer, update polymer to know that the protein is stuck to it
		if (grid.getCell(newMove) != -1) {
			int index = grid.getCell(newMove);
			poly.updateOccupation(index, 1);
		}
	}

	private void updateEndProtein(int index) {

		//Protein stays on
		if (rndm.nextDouble() < probStayOnEnd) {

			//Protein goes forwards along the DNA chain
			if (index == 0) {
				pos = poly.getPoly((index+1)).getPos();
				poly.updateOccupation(index+1, 1);
			}

			//Protein goes backwards along the DNA chain
			else {
				pos = poly.getPoly((index-1)).getPos();
				poly.updateOccupation(index-1, 1);
			}
		}

		//Protein jumps off
		else {
			//Get relative map from END TO NEAREST
			Map map;
			if (index == 0) {
				map = Map.getRelativeMap(poly.getPoly(index).getPos(), poly.getPoly(index+1).getPos());
			}
			else { map = Map.getRelativeMap(poly.getPoly(index).getPos(), poly.getPoly(index-1).getPos()); }

			for (int i=0; i<6; i++) {
				if (Map.compareMaps(map, endMaps[i]) == true) {
					conf = endConfs[i]; break;	//Does break break out of if or out of for loop?
				}
			}

			/*
			 * Probability of either move in configuration is
			 * 1/5. Choose one at random. Add the relative move to the
			 * position of nearest to get the new move of the end
			 * monomer. Try the move: If cell is empty, perform move.
			 */
			Map[] relativeMoves = conf.getPossibleMoves();
			int random = (int) (rndm.nextDouble()*5);
			Cell newMove = Cell.Add(relativeMoves[random], pos);
			pos = newMove;

			//In case newMove is on polymer, update polymer to know that the protein is stuck to it
			if (grid.getCell(newMove) != -1) {
				int newIndex = grid.getCell(newMove);
				poly.updateOccupation(newIndex, 1);
			}
		}
	}

	private void updateInternalProtein(int index) {
		if (rndm.nextDouble() < probStayOnInternal) {

			//Go to the next monomer in chain
			if(rndm.nextDouble() < 0.5) {
				pos = poly.getPoly(index+1).getPos();
				poly.updateOccupation(index+1, 1);
			}
			//Go to the previous monomer in chain
			else {
				pos = poly.getPoly(index-1).getPos();
				poly.updateOccupation(index-1, 1);
			}

		}

		//Jump off polymer
		else {

			Map[] map = Map.getRelativeMap(poly.getPoly(index-1).getPos(),
					poly.getPoly(index).getPos(), poly.getPoly(index+1).getPos());

			for (int i=0; i<30 ; i++) {
				if (Map.compareMaps(map, internalMaps[i]) == true) {
					conf = internalConfs[i];  break;	//Does break break out of if or for loop?
				}
			}

			/*
			 * Probability of either move in configuration is
			 * 1/4. Choose one at random. Add the relative move to the
			 * position of nearest to get the new move of the end
			 * monomer. Try the move: If cell is empty, perform move.
			 */
			Map[] relativeMoves = conf.getPossibleMoves();
			int random = (int) (rndm.nextDouble()*4);
			Cell newMove = Cell.Add(relativeMoves[random], pos);
			pos = newMove;

			//In case newMove is on polymer, update polymer to know that the protein is stuck to it
			if (grid.getCell(newMove) != -1) {
				int newIndex = grid.getCell(newMove);
				poly.updateOccupation(newIndex, 1);
			}
		}
	}

	public boolean getTargetStatus() { return targetFound; }
	public int getOnDNA() { return onDNA; }
	public void setPosition(Cell newPos) { pos = newPos; }
	public Cell getPos() { return pos; }

	public String toVMD() {
		return "Protein " + pos.toString() + "\n";
	}


	/*
	 * Method for setting affinity.
	 * Computes probabilites for 1d diffusion on polymer.
	 */
	public void setAffinity(double newAff) {
		//Compute probabilites for 1d diffusion at internal monomer
		affinity = newAff;
		double totalProb = (1.0/3.0)*1.0 + (2.0/3.0)*Math.exp(-affinity);
		probStayOnInternal = (1.0/3.0)/totalProb;

		//Compute probabilities for 1d diffusion at end monomer
		totalProb = (1.0/6.0)*1.0 + (5.0/6.0)*Math.exp(-affinity);
		probStayOnEnd = (1.0/6.0)/totalProb;
	}
}
