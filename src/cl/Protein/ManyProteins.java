package cl.Protein;

import java.util.Random;

import cl.DNA.Grid;
import cl.DNA.PolymerManyProteins;
import cl.Lattice.Cell;

public class ManyProteins {

	//Internal variables
	private Proteins[] proteinArray;
	private Grid protGrid;
	private int N;

	public ManyProteins(int numOfProteins, int boxSize, PolymerManyProteins p, Random r, double aff, double target) {

		N = numOfProteins;
		protGrid = new Grid(boxSize);

		if (N!= 0) {
			proteinArray = new Proteins[N];
			for (int ID=0; ID<N; ID++) {
				//Do while loop makes sure two proteins aren't placed on top of each other
				do {
					proteinArray[ID] = new Proteins(boxSize, p, r, aff, target);	//place at random in box
				} while(protGrid.getCell(proteinArray[ID].getPos()) != -1);
				proteinArray[ID].setID(ID);
				proteinArray[ID].setGrid(protGrid);
				protGrid.update(proteinArray[ID].pos, ID);	//Add protein to grid
			}
		}
	}

	public void update() {
		for (int i=0; i<N; i++) {
			proteinArray[i].update();
		}
	}

	public Protein getProt(int index) { return proteinArray[index]; }
	public int getProtGrid(Cell c) { return protGrid.getCell(c); }
	public void updateProtGrid(Cell position, int newValue) { protGrid.update(position, newValue); }
	public void doMove(int index, Cell newMove) { proteinArray[index].doMove(newMove); }

	public boolean getTargetStatus() {
		for (int i=0; i<N; i++) {
			if (proteinArray[i].getTargetStatus() == true) {
				return true;
			}
		}
		return false;
	}

	public String toVMD() {
		String output = "";
		for (int i=0; i<N; i++) {
			output += "Protein" + i + " " + proteinArray[i].pos.toString() + "\n";
		}
		return output;
	}
}
