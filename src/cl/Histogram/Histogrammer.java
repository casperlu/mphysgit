package cl.Histogram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Histogrammer {

	/**
	 * Reads in data file and bins it
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String path = "HistogramAff6.0Seeds2000.dat";
		BufferedReader reader = new BufferedReader(new FileReader(path));
		Scanner scan = new Scanner(reader);
		
		int count = 0;
		int max = 0;
		int current;
		while (scan.hasNext() == true) {
			count ++;
			current = scan.nextInt();
			if (current > max) {
				max = current;
			}
		}
		scan.close();

		
		//Create array of data
		int[] data = new int[count];
		
		//Set up scanner again
		BufferedReader reader2 = new BufferedReader(new FileReader(path));
		Scanner scan2 = new Scanner(reader2);
		
		//Fill log array with log values
		for (int i=0; i<count; i++) {
			data[i] = scan2.nextInt();
		}
		
		
		
		//Histogram data
		int binSize = 1000;
		int numOfBins = max/binSize + 1;
		int[] hist = new int[numOfBins];
		
		//Reset histogram to have values of 1 everywhere
		for (int i=0; i<numOfBins; i++) { hist[i] = 1; }
		
		//Compute histogram
		int binNumber;
		for (int i=0; i<count; i++) {
			binNumber = data[i]/binSize;
			hist[binNumber] += 1;
		}
		
		String outPath = "Data/Histogram/Hist6.0Log.dat";
		PrintWriter printer = new PrintWriter(new FileWriter(outPath));
		
		for (int i=0; i<numOfBins; i++) {
			printer.println((i*binSize + 0.5*binSize) + "\t" + Math.log(hist[i]));
		}
		
		scan2.close();
		printer.close();	
	}
}