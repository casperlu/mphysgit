package cl.Random;

import java.util.Random;

public class BMgenerator {
	
	//Constants
	final static double twoPi = 2*Math.PI;
	
	public static double[] generateGaussian(int N, double mean, double stDev, double cutOff, long seed) {
		Random uniform = new Random(seed);
		double[] randoms = new double[N];
		
		for (int i=0, j=0; i<(N/2); i++) {
			
			//Generate two uniformly distributed numbers x1,x2
			double x1 = uniform.nextDouble();
			double x2= uniform.nextDouble();
			
			//Use these to get two Gaussian numbers z1,z2
			double z1 = Math.sqrt(-2*Math.log(x1)) * Math.sin(twoPi*x2);
			double z2 = Math.sqrt(-2*Math.log(x1)) * Math.cos(twoPi*x2);
			
			//Adjust mean and standard deviation
			randoms[j++] = mean + z1*stDev;
			randoms[j++] = mean + z2*stDev;
		}
		
		//In case N odd we need to generate an extra number
		if (N%2 == 1) {
			
			//Generate a single random number
			double z = generateOne(uniform);
			
			//Adjust mean and standard deviation
			randoms[N-1] = mean + z*stDev;
		}
		
		
		/*
		 * Make sure all gaussian numbers are within [-cutOff,+cutoff]
		 */
		boolean outSideRange = true;
		while (outSideRange == true) {
			outSideRange = false;
			for (int i=0; i<N; i++) {
				if (Math.abs(randoms[i]) > cutOff) {
					randoms[i] = mean + generateOne(uniform)*stDev;
					outSideRange = true;
				}
			}
		}
		
		return randoms;
	}

	private static double generateOne(Random uniform) {
		
		//Generate two uniformly distributed numbers x1,x2
		double x1 = uniform.nextDouble();
		double x2= uniform.nextDouble();
		
		//Return only one random gaussian number
		return Math.sqrt(-2*Math.log(x1)) * Math.sin(twoPi*x2);
	}
}
