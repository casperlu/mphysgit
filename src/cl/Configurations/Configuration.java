package cl.Configurations;

import cl.Lattice.Map;

public interface Configuration {

	public abstract Map[] getPossibleMoves();
}
