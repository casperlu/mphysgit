package cl.Configurations;

import cl.Lattice.Map;

public enum CrankYZConfiguration implements Configuration {

	YZ1 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,1);
			Map m2 = new Map(0,0,2);
			Map m3 = new Map(-1,0,1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	YZ2 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,-1);
			Map m2 = new Map(0,0,-2);
			Map m3 = new Map(-1,0,-1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	YZ3 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(-1,1,0);
			Map m2 = new Map(0,2,0);
			Map m3 = new Map(-1,1,0);
			return new Map[] {m1, m2, m3};
		}
	},
	
	YZ4 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,-1,0);
			Map m2 = new Map(0,-2,0);
			Map m3 = new Map(1,-1,0);
			return new Map[] {m1, m2, m3};
		}
	};
	
	/*
	 * Abstract method that when called gives all the possible
	 * moves for a certain configuration. The method must be 
	 * implemented in all enum constants.
	 */
	public abstract Map[] getPossibleMoves();
	
	/*
	 * Definition of relative maps for crankshaft configurations.
	 * See notebook drawings.
	 */
	private final static Map[][] cranksYZ = new Map[][] {
			{new Map(0,0,-1), new Map(0,1,0), new Map(0,0,1)},	//YZ1
			{new Map(0,0,1), new Map(0,1,0), new Map(0,0,-1)},	//YZ2
			{new Map(0,-1,0), new Map(0,0,1), new Map(0,1,0)},	//YZ3
			{new Map(0,1,0), new Map(0,0,1), new Map(0,-1,0)},	//YZ4
			{new Map(0,0,-1), new Map(0,-1,0), new Map(0,0,1)},	//YZ5
			{new Map(0,0,1), new Map(0,-1,0), new Map(0,0,-1)},	//YZ6
			{new Map(0,-1,0), new Map(0,0,-1), new Map(0,1,0)},	//YZ7
			{new Map(0,1,0), new Map(0,0,-1), new Map(0,-1,0)}	//YZ8
	};
	
	public static Map[][] getCranksYZ() { return cranksYZ; }
	
	/*
	 * Array of enums
	 */
	private final static CrankYZConfiguration[] crankYZArray = new CrankYZConfiguration[] {
		YZ1,
		YZ2,
		YZ3,
		YZ4,
		YZ1,	//The following four
		YZ2,	//are the same as the
		YZ3,	//first four.
		YZ4		//Think about it.
	};
	
	public static CrankYZConfiguration[] getCrankConfigurations() {
		return crankYZArray;
	}
	
}
