package cl.Configurations;

import cl.Lattice.Map;

public enum EndConfiguration implements Configuration {

	ENDMONOMER {
		public Map[] getPossibleMoves() {
			
			
			Map m1 = new Map(-1, 0, 0);
			Map m2 = new Map(1, 0, 0);
			Map m3 = new Map(0, -1, 0);
			Map m4 = new Map(0, 1, 0);
			Map m5 = new Map(0, 0, -1);
			Map m6 = new Map(0, 0, 1);
			
			return new Map[] {m1,m2,m3,m4,m5,m6};
		}
	};
	
	/*
	 * Abstract method that when called gives all the possible
	 * moves for a certain configuration. The method must be 
	 * implemented in all enum constants.
	 */
	public abstract Map[] getPossibleMoves();
}
