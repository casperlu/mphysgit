package cl.Configurations;

import cl.Lattice.Map;

public enum CrankXYConfiguration implements Configuration {

	XY1 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,1);
			Map m2 = new Map(0,2,0);
			Map m3 = new Map(0,1,-1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	XY2 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,-1,1);
			Map m2 = new Map(0,-2,0);
			Map m3 = new Map(0,-1,-1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	XY3 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,1);
			Map m2 = new Map(2,0,0);
			Map m3 = new Map(1,0,-1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	XY4 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(-1,0,1);
			Map m2 = new Map(-2,0,0);
			Map m3 = new Map(-1,0,-1);
			return new Map[] {m1, m2, m3};
		}
	};
	
	/*
	 * Abstract method that when called gives all the possible
	 * moves for a certain configuration. The method must be 
	 * implemented in all enum constants.
	 */
	public abstract Map[] getPossibleMoves();
	
	/*
	 * Definition of relative maps for crankshaft configurations.
	 * See notebook drawings.
	 */	
	private final static Map[][] cranksXY = new Map[][] {
		
		{new Map(0,-1,0), new Map(1,0,0), new Map(0,1,0)},	//XY1
		{new Map(0,1,0), new Map(1,0,0), new Map(0,-1,0)},	//XY2
		{new Map(-1,0,0), new Map(0,1,0), new Map(1,0,0)},	//XY3
		{new Map(1,0,0), new Map(0,1,0), new Map(-1,0,0)},	//XY4
		{new Map(0,-1,0), new Map(-1,0,0), new Map(0,1,0)},	//XY5
		{new Map(0,1,0), new Map(-1,0,0), new Map(0,-1,0)}, //XY6
		{new Map(-1,0,0), new Map(0,-1,0), new Map(1,0,0)},	//XY7
		{new Map(1,0,0), new Map(0,-1,0), new Map(-1,0,0)}	//XY8
};
	
	public static Map[][] getCranksXY() { return cranksXY; }
	
	/*
	 * Array of enums
	 */
	private final static CrankXYConfiguration[] crankXYArray = new CrankXYConfiguration[] {
		XY1,
		XY2,
		XY3,
		XY4,
		XY1,	//The following four
		XY2,	//are the same as the
		XY3,	//first four.
		XY4		//Think about it.
	};
	
	public static CrankXYConfiguration[] getCrankConfigurations() {
		return crankXYArray;
	}
}
