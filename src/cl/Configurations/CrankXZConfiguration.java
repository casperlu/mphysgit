package cl.Configurations;

import cl.Lattice.Map;

public enum CrankXZConfiguration implements Configuration {

	XZ1 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,-1,1);
			Map m2 = new Map(0,0,2);
			Map m3 = new Map(0,1,1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	XZ2 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,-1,-1);
			Map m2 = new Map(0,0,-2);
			Map m3 = new Map(0,1,-1);
			return new Map[] {m1, m2, m3};
		}
	},
	
	XZ3 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,-1,0);
			Map m2 = new Map(2,0,0);
			Map m3 = new Map(1,1,0);
			return new Map[] {m1, m2, m3};
		}
	},
	
	XZ4 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(-1,-1,0);
			Map m2 = new Map(-2,0,0);
			Map m3 = new Map(-1,1,0);
			return new Map[] {m1, m2, m3};
		}
	};
	
	/*
	 * Abstract method that when called gives all the possible
	 * moves for a certain configuration. The method must be 
	 * implemented in all enum constants.
	 */
	public abstract Map[] getPossibleMoves();
	
	/*
	 * Definition of relative maps for crankshaft configurations.
	 * See notebook drawings.
	 */
	private final static Map[][] cranksXZ = new Map[][] {
			{new Map(0,0,-1), new Map(1,0,0), new Map(0,0,1)},	//XZ1
			{new Map(0,0,1), new Map(1,0,0), new Map(0,0,-1)},	//XZ2
			{new Map(-1,0,0), new Map(0,0,1), new Map(1,0,0)},	//XZ3
			{new Map(1,0,0), new Map(0,0,1), new Map(-1,0,0)},	//XZ4
			{new Map(0,0,-1), new Map(-1,0,0), new Map(0,0,1)},	//XZ5
			{new Map(0,0,1), new Map(-1,0,0), new Map(0,0,-1)},	//XZ6
			{new Map(-1,0,0), new Map(0,0,-1), new Map(1,0,0)},	//XZ7
			{new Map(1,0,0), new Map(0,0,-1), new Map(-1,0,0)}	//XZ8
	};
	
	public static Map[][] getCranksXZ() { return cranksXZ; }
	
	/*
	 * Array of enums
	 */
	private final static CrankXZConfiguration[] crankXZArray = new CrankXZConfiguration[] {
		XZ1,
		XZ2,
		XZ3,
		XZ4,
		XZ1,	//The following four
		XZ2,	//are the same as the
		XZ3,	//first four.
		XZ4		//Think about it.
	};
	
	public static CrankXZConfiguration[] getCrankConfigurations() {
		return crankXZArray;
	}
}
