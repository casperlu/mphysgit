package cl.Lattice;

import java.util.Random;

/*
 * Cell class for a monomer. The cell specifies the position of
 * the monomer. The class also provides method for finding random
 * adjacent cells and for map + cell additions.
 */
public class Cell {

	//Internal variables
	private int[] pos;
	private int box;

	/*
	 * Explicit constructor 1. Takes into account boundary conditions.
	 */
	public Cell(int x, int y, int z, int boxSize) {
		box = boxSize;
		pos = new int[] {(x+box)%box,(y+box)%box,(z+box)%box};
	}

	/*
	 * Explicit constructor 2. Takes into account boundary conditions.
	 */
	public Cell(int[] pos, int boxSize) {
		box = boxSize;
		this.pos = new int[] {(pos[0]+box)%box, (pos[1]+box)%box, (pos[2]+box)%box};
	}

	/*
	 * Random constructor. Takes into account boundary conditions.
	 */
	public Cell(int boxSize, Random rndm) {
		box = boxSize;
		int x = (int) (rndm.nextDouble()*box);
		int y = (int) (rndm.nextDouble()*box);
		int z = (int) (rndm.nextDouble()*box);
		pos = new int[] {x,y,z};
	}

	public static Cell getRandomNeighbour(Cell c, Random rndm) {
		int boxSize = c.getBox();

		//Displacements
		int dx = 0, dy = 0, dz = 0;
		
		//Generate random displacements until not (0,0,0)
		while (dx == 0 && dy ==0 && dz == 0) {
			dx = rndm.nextInt(3) -1;	//Generate dx in [0:3) = [0,1,2] and then subtract one to have dx in [-1,0,1]
			dy = rndm.nextInt(3) -1;
			dz = rndm.nextInt(3) -1;
		}

		//New coords
		int[] newCoords = new int[3];
		newCoords[0] = c.getCoord(0) + dx;
		newCoords[1] = c.getCoord(1) + dy;
		newCoords[2] = c.getCoord(2) + dz;

		//Return the new neighbouring cell
		Cell neighbour = new Cell(newCoords, boxSize);
		return neighbour;
	}

	//Getters
	public int getCoord(int i) { return pos[i]; }
	public int[] getCoords() { return pos; }
	public int getBox() { return box; }

	/*
	 * Produce a new lattice point at a random position
	 * immediately next to the previous cell.
	 */
	//	public static Cell randomAdjacentCell(Cell lastCell, Random rndm) {
	//	
	//		//Generate a random step direction, e.g. (0,-1,0) or (1,0,0)
	//				int[] step = new int[] {0,0,0};
	//				int random = (int) (rndm.nextDouble()*6);
	//				if (random == 0) { step[0] = -1; }
	//				else if(random == 1) { step[0] = 1; }
	//				else if(random == 2) { step[1] = -1; }
	//				else if(random == 3) { step[1] = 1; }
	//				else if(random == 4) { step[2] = -1; }
	//				else { step[2] = 1; }
	//				Cell stepCell = new Cell(step, lastCell.getBox());	//THIS IS PROBABLY WRONG
	//
	//				return Add(lastCell, stepCell);
	//	}

	/*
	 * Coordinate wise addition of cells. Take into account boundary condition
	 */
	//	public static Cell Add(Cell a, Cell b) {
	//		int[] position = new int[3];
	//		int boxSize = a.getBox();
	//		for (int i=0; i<3; i++) { position[i] = a.getCoord(i) + b.getCoord(i); }
	//		
	//		return new Cell(position, boxSize);
	//	}

	/*
	 * Add a Map of a certain move to the current position of a Cell c.
	 * Return the new Cell.
	 */
	public static Cell Add(Map move, Cell c) {
		int[] position = new int[3];
		int boxSize = c.getBox();

		//MAPS DO NOT OBEY BOUNDARY CONDITIONS. HENCE THIS NEED TO BE CORRECTED FOR:
		for (int i=0; i<3; i++) {
			if (move.getCoord(i) < -boxSize/2) { move.setCoord(i, boxSize + move.getCoord(i)); }
		}

		for (int i=0; i<3; i++) { position[i] = move.getCoord(i) + c.getCoord(i); }

		return new Cell(position, boxSize);
	}

	/*
	 * Add two Maps to the position of a Cell. Return new Cell.
	 */
	public static Cell Add(Map move1, Map move2, Cell c) {
		int[] position = new int[3];
		int boxSize = c.getBox();

		//MAPS DO NOT OBEY BOUNDARY CONDITIONS. HENCE THIS NEED TO BE CORRECTED FOR:
		for (int i=0; i<3; i++) {
			if (move1.getCoord(i) < -boxSize/2) { move1.setCoord(i, boxSize + move1.getCoord(i)); }
		}

		for (int j=0; j<3; j++) {
			if (move2.getCoord(j) < -boxSize/2) { move2.setCoord(j, boxSize + move2.getCoord(j));  }
		}

		for (int k=0; k<3; k++) { position[k] = move1.getCoord(k) + move2.getCoord(k) + c.getCoord(k); }

		return new Cell(position, boxSize);
	}

	/*
	 * Method for checking if four cells are in the same level.
	 * Returns level (YZ=0, XZ=1, XY=2) or -1 for not in same level.
	 */
	public static int checkSamePlane(Cell a, Cell b, Cell c, Cell d) {

		for (int i=0; i<3; i++) {
			if (a.getCoord(i) == b.getCoord(i) 
					&& b.getCoord(i) == c.getCoord(i)
					&& c.getCoord(i) == d.getCoord(i)) {
				return i;
			}
		}

		return -1;
	}

	public String toString() {
		return "" + pos[0] + " " + pos[1] + " " + pos[2]; 
	}
}
