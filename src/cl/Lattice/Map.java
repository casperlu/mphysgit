package cl.Lattice;

/*
 * Map class specifies moves on the lattice.
 */
public class Map {

	//Internal variable
	int[] map;
	
	/*
	 * Explicit constructor. Maps don't obey boundary conditions.
	 */
	public Map(int x, int y, int z) {
		map = new int[]{x,y,z};
	}
	
	/*
	 * Returns relative Map between Cell A & B
	 * That is returns B-A (THIS IS IMPORTANT!)
	 */

	//Now with boundary conditions. But there shouldn't really by BC in a map?
	//Consider rethinking code structure.
	public static Map getRelativeMap(Cell cellA, Cell cellB) {
		int box = cellA.getBox();
		
		int[] a = cellA.getCoords();
		int[] b = cellB.getCoords();
		
		int x = (b[0] - a[0]) < (-box/2)? (box + b[0] -a[0]) : (b[0]-a[0]);
		int y = (b[1] - a[1]) < (-box/2)? (box + b[1] -a[1]) : (b[1]-a[1]);
		int z = (b[2] - a[2]) < (-box/2)? (box + b[2] -a[2]) : (b[2]-a[2]);
		
		x = (b[0] - a[0]) > (box/2)? (-box + b[0] -a[0]) : x;
		y = (b[1] - a[1]) > (box/2)? (-box + b[1] -a[1]) : y;
		z = (b[2] - a[2]) > (box/2)? (-box + b[2] -a[2]) : z;
		
		return new Map(x,y,z);
	}
	
	/*
	 * Creates a Map of relative distances between three Cells.
	 */
	public static Map[] getRelativeMap(Cell a, Cell b, Cell c) {
		Map[] m = new Map[2];
		m[0] = getRelativeMap(a,b);	//b-a
		m[1] = getRelativeMap(b,c);	//c-b
		return m;
	}
	
	/*
	 * Creates a Map of relative distances between four Cells.
	 * E.g. for four monomers forming a well (square without top row):
	 * {0,-1,0}, {1,0,0}, {0,1,0}.
	 */
	public static Map[] getRelativeMap(Cell a, Cell b, Cell c, Cell d) {
		Map[] m = new Map[3];
		m[0] = getRelativeMap(a,b);	//b-a
		m[1] = getRelativeMap(b,c);	//c-b
		m[2] = getRelativeMap(c,d);	//d-c
		return m;
	}
	
	//Get the length of a map
	public double getDist() {
		return Math.sqrt(map[0]*map[0] + map[1]*map[1] + map[2]*map[2]);
	}
	
	//Getters and setters
	public int getCoord(int i) { return map[i]; }
	public void setCoord(int i, int value) { map[i] = value; }
	
	/*
	 * Compare if two Maps are identical.
	 */
	public static boolean compareMaps(Map a, Map b) {
		boolean result = true;
		for (int i=0; i<3; i++) {
			if (a.getCoord(i) != b.getCoord(i)) { result = false; break;}
		}
		return result;
	}
	
	/*
	 * Compare if two arrays of Maps are identical.
	 */
	public static boolean compareMaps(Map[] a, Map[] b){
		boolean result = true;
		for (int i=0; i<a.length; i++) {
			if (compareMaps(a[i], b[i]) == false) { result = false; break; }
		}
		return result;
	}
}
