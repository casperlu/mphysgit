package cl.PWM;

public class PWM {

	//Internal variables
	private int L;
	private NucleotideValues[] pwm;
	
	public PWM(int lengthOfMotif) {
		L = lengthOfMotif;
		pwm = new NucleotideValues[L];	//A,C,G,T
		
		//Set to zero to avoid null-pointer exceptions
		for (int i=0; i<L; i++) {
			pwm[i] = new NucleotideValues();
		}
	}
	
	/*
	 * Base = 0 --> A
	 * Base = 1 --> C
	 * Base = 2 --> G
	 * Base = 3 --> T
	 */
	public void setPWM(int num, char base, double value) {
		pwm[num].setNucleotides(base, value);
	}
	
	public double getPWM(int num, char base) { 
		return pwm[num].getNucleotides(base); }
}
