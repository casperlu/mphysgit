package cl.PWM;

public class NucleotideValues {

	//Private variables
	private double A,C,G,T;
	
	public NucleotideValues(double valueA, double valueC, double valueG, double valueT) {
		A = valueA;
		C = valueC;
		G = valueG;
		T = valueT;
	}
	
	public NucleotideValues() {
		A = 0;
		C = 0;
		G = 0;
		T = 0;
	}
	
	public void setNucleotides(char nt, double value) {
		if (nt == 'A') { A = value; }
		else if (nt == 'C') { C = value; }
		else if (nt == 'G') { G = value; }
		else if (nt == 'T') { T = value; }
		else { System.out.println("EXCEPTION HERE: NUCLEOTIDE IS NOT A, C, G, or T!!"); }
	}
	
	public double getNucleotides(char nt) {
		if (nt == 'A') { return A; }
		else if (nt == 'C') { return C; }
		else if (nt == 'G') { return G; }
		else if (nt == 'T') { return T; }
		else { 
			System.out.println("EXCEPTION HERE: NUCLEOTIDE IS NOT A, C, G, or T!!");
			return Double.NaN;
		}
	}
}
