package cl.PWM;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PWMTester {

	//Internal variables
	private final static char[] nucleotides = {'A','C','G','T'};
	
	/**
	 * Test of PWMProducer
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		//Set up motifs and feed them to producer
		//String[] bindingMotifsOfLacI = new String[] {"AAATGTGAGCGAGTAACAACC", "AATTGTTATCCGCTCACAATT", "GGCAGTGAGCGCAACGCAATT"}; //,"AATTGCGTTGCGCTCACTGCC"};
		String[] bindingMotifsOfLacI = new String[] {"AAATGTNNNNNNNNNACAACC", "AATTGTNNNNNNNNNACAATT", "GGCAGTNNNNNNNNNGCAATT"}; //,"AATTGCNNNNNNNNNACTGCC"};
		PWMProducer producer = new PWMProducer(bindingMotifsOfLacI);
		
		//Compute PWM. Takes epsilonStar as argument
		PWM pwm = producer.computePWM(1.0000);
		
		//Write PWM to file
		PrintWriter output1 = new PrintWriter(new FileWriter("LacIPWM.out"));
		for (char j : nucleotides) {
			output1.print(j + "\t");
		}
		output1.println("");
		for (int i=0; i<bindingMotifsOfLacI[0].length(); i++) {
			for (char j : nucleotides) {
				output1.print(pwm.getPWM(i, j) + "\t");
			}
			output1.println("");
		}
		output1.close();
	}
}
