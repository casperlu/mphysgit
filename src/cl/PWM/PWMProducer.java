package cl.PWM;


public class PWMProducer {

	//Internal variables
	private char[][] motifs;
	private double nu_A = 0.246;
	private double nu_C = 0.254;
	private double nu_G = 0.254;
	private double nu_T = 0.246;
	private NucleotideValues nu = new NucleotideValues(nu_A, nu_C, nu_G, nu_T);
	private final char[] nucleotides = {'A','C','G','T'}; 
	private int N,L;
	private PWM pwm;

	/*
	 * Constructor takes array of high-affinity binding sites and
	 * GenomeManager instance. This instance must be already set up
	 * with the right genome data etc.
	 */
	public PWMProducer(String[] bindingMotifs) {
		N = bindingMotifs.length;
		motifs = new char[N][];
		for (int i=0; i<N; i++) {
			motifs[i] = bindingMotifs[i].toCharArray();
		}
		L = motifs[0].length;
		pwm = new PWM(L);
	}

	public PWM computePWM(double epsilonStar) {
		double pseudoCount = N;

		//Loop over all basepairs in motifs
		for (int i=0; i<L; i++)

			//Loop over all nucleotides (A,C,G,T)
			for (char j : nucleotides) {
				double n_j = 0;

				//Loop over all motifs
				for (int k=0; k<N; k++) {
					if (motifs[k][i] == j) {
						n_j++;
					}
					else if (motifs[k][i] == 'N') {
						n_j += 0.25;
					}
				}
				double nu_j = (n_j + pseudoCount*nu.getNucleotides(j)) / (N + pseudoCount);
				pwm.setPWM(i, j, epsilonStar*Math.log(nu_j/nu.getNucleotides(j)));
			}

		return pwm;
	}
}
