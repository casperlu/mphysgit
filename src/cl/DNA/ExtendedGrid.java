package cl.DNA;

import cl.Lattice.Cell;

/*
 * ExtendedGrid works as hashtable for polymer. The value at each cell defines the
 * number of the monomer in the array.
 * The value -1 signals an empty cell.
 * 
 * A protein placed in an extended grid occupies more than a single monomer.
 * Additionally to the site (x,y,z) it also occupies (x+1,y,z),(x-1,y,z),(x,y+1,z),
 * (x,y-1,z),(x,y,z+1), and (x,y,z-1).
 */
public class ExtendedGrid extends Grid {

	//Constructor. Super call reset all cells in grid to zero.
	public ExtendedGrid(int boxLength) {
		super(boxLength);
	}

	/*
	 * Update a certain cell in the grid.
	 * Takes into account boundary conditions.
	 * Updates all nearest neighbour sites to same value.
	 */
	@Override
	public void update(Cell c, int value) {

		int[][] displacements = {{0,0,0},{1,0,0},{-1,0,0},{0,1,0},{0,-1,0},{0,0,1},{0,0,-1}};

		for (int i=0; i<7; i++) {
			grid[(c.getCoord(0) + displacements[i][0] + box)%box]
					[(c.getCoord(1) + displacements[i][1] + box)%box]
					[(c.getCoord(2) + displacements[i][2] + box)%box] = value;
		}
	}
}
