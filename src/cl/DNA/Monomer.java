package cl.DNA;

import java.util.Random;

import cl.Lattice.Cell;

public class Monomer {
	
	//Internal variables
	//protected int[] pos;
	private Cell pos;
	private int box;
	private int target;
	private int occupation;		//1=siteOccupied by protein, 0 empty
	
	/*
	 * Explicit constructor. Takes into account boundary
	 * conditions.
	 */
	public Monomer(int x, int y, int z, int boxSize) {
		box = boxSize;
		pos = new Cell(x,y,z,box);
		occupation = -1;
	}
	
	/*
	 * Explicit constructor2. Takes into account boundary
	 * conditions.
	 */
	public Monomer(int[] pos, int boxSize) {
		box = boxSize;
		this.pos = new Cell(pos, box);
		occupation = -1;
	}
	
	/*
	 * Explicit constructor3. Takes into account boundary
	 * conditions.
	 */
	public Monomer(Cell c, int boxSize) {
		box = boxSize;
		
		//Use constructor to be sure boundary conditions are ok.
		//Probably unnecessary.
		pos = new Cell(c.getCoords(), box);
		occupation = -1;
	}
	
	/*
	 * Construct random monomer
	 */
	public Monomer(Random rndm, int boxSize) {
		box = boxSize;
		
		//Place monomer at a random position within box
		int i = (int) (rndm.nextDouble()*box);
		int j = (int) (rndm.nextDouble()*box);
		int k = (int) (rndm.nextDouble()*box);
		pos = new Cell(i, j, k, box);
		occupation = -1;
	}
	
	//Get the position of the monomer
	public Cell getPos() {
		return pos;
	}
	
	//Set the position
	public void setPos(Cell newPos) {
		pos = newPos;
	}
	
	//toString method
	public String toString() {
		String output = "Monomer position: " + pos.getCoord(0) + " "
				+ pos.getCoord(1) + " " + pos.getCoord(2);
		return output;
	}
	
	//Is this still relevant?
	public String printPos() {
		return "" + pos.getCoord(0) + " "+ pos.getCoord(1) + " " + pos.getCoord(2);
	}

	protected void setTarget(int value) { target = value; }
	public int getTarget() { return target; }
	
	//occupation == -1 (empty), 1 (protein #1), 2 (protein #2), 28 (protein # 28), etc.
	public void setOccupation(int value) { occupation = value; }
	public int getOccupation() { return occupation; }
}
