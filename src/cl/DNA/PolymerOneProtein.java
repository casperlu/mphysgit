package cl.DNA;

import java.util.Random;

import cl.Lattice.Cell;
import cl.Protein.Protein;

public class PolymerOneProtein extends Polymer {

	//Internal variables
	private Protein prot;
	
	public PolymerOneProtein(int boxLength, int DNALength, Random r) {
		super(boxLength, DNALength, r);
		// TODO Auto-generated constructor stub
	}

	
	/*
	 * Overrides tryMove for a polymer that is in a system with one
	 * protein.
	 */
	@Override
	protected void tryMove(Cell newCell, Cell oldCell, int index) {
		if (grid.getCell(newCell) == -1) {

				poly[index].setPos(newCell);
				grid.update(oldCell, -1);
				grid.update(newCell, index);
				
				//In case there is a protein on the monomer, move it with the monomer
				if (poly[index].getOccupation() == 1) {
					prot.setPosition(newCell);
				}
		}
		//Otherwise move is not possible.
	}
	

	/*
	 * Getters and setters related to target of protein
	 * and occupation of monomer.
	 */
	public int getTarget(int i) { return poly[i].getTarget(); }

	public void setTarget(int index, int value) {
		poly[index].setTarget(value);
	}
	public void updateOccupation(int index, int value) { poly[index].setOccupation(value); }
	
	public void setProtein(Protein p) { prot = p; }
}
