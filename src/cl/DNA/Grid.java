package cl.DNA;

import cl.Lattice.Cell;

/*
 * Grid works as hashtable for polymer. The value at each cell defines the
 * number of the monomer in the array.
 * The value -1 signals an empty cell.
 */
public class Grid {
	
	//Internal variables
	protected int[][][] grid;
	protected int box;
	
	public Grid(int boxLength) {
		box = boxLength;
		grid = new int[box][box][box];
		for (int i=0; i<box; i++) {
			for (int j=0; j<box; j++) {
				for (int k=0; k<box; k++) {
					grid[i][j][k] = -1;		//Initially all cells are empty
				}
			}
		}
	}
	
	/*
	 * Update a certain cell in the grid.
	 * Takes into account boundary conditions.
	 */
	public void update(Cell c, int value) {
		grid[(c.getCoord(0)+box)%box][(c.getCoord(1)+box)%box]
				[(c.getCoord(2)+box)%box] = value;
	}
	
	
	/*
	 * Get the value of a cell.
	 * Takes into account boundary conditions
	 */
	//TODO: Boundary conditions should be unnecessary once finished updating code. Check if this is true.
	public int getCell(Cell c) {
		return grid[(c.getCoord(0)+box)%box][(c.getCoord(1)+box)%box]
				[(c.getCoord(2)+box)%box];
	}
}
