package cl.DNA;

import java.util.Random;

import cl.Configurations.*;
import cl.Lattice.Cell;
import cl.Lattice.Map;

public class Polymer {

	//Internal variables
	protected Monomer[] poly;
	protected int N, box;
	public Grid grid;
	protected Random rndm;
	protected Configuration conf = null;

	/*
	 * These arrays holds all the maps and moves for crankshaft moves.
	 * They are constant values.
	 */
	protected final Map[][] cranksXY = CrankXYConfiguration.getCranksXY();
	protected final CrankXYConfiguration[] CrankXYConfs = CrankXYConfiguration.getCrankConfigurations();
	protected final Map[][] cranksXZ = CrankXZConfiguration.getCranksXZ();
	protected final CrankXZConfiguration[] CrankXZConfs = CrankXZConfiguration.getCrankConfigurations();
	protected final Map[][] cranksYZ = CrankYZConfiguration.getCranksYZ();
	protected final CrankYZConfiguration[] CrankYZConfs = CrankYZConfiguration.getCrankConfigurations();

	/*
	 * Constructor creates an array of non-initialised monomers.
	 * A grid is constructed to serve as a hash table.
	 */
	public Polymer(int boxLength, int DNALength, Random r) {
		poly = new Monomer[DNALength];
		N = DNALength;
		box = boxLength;
		grid = new Grid(box);
		rndm = r;
	}

	/*
	 * Initialises the monomers in a random chain in the box. Takes seed.
	 */
	public void initialise(int x, int y, int z) {

		//Place first monomer at random in box
		if (x == -1 && y==-1 && z==-1) {
		poly[0] = new Monomer(rndm, box);
		}
		else { poly[0] = new Monomer(x,y,z,box); }

		/*
		 * Update grid accordingly: Set lattice cell
		 * of monomer to occupied by poly[0], hence 0
		 */
		grid.update(poly[0].getPos(), 0);

		/*
		 * Add subsequent monomers in a random chain after each other:
		 * newRandomCell produces a lattice point next to the previous
		 * monomer. If this newCell is not already on the grid, the next
		 * monomer is placed at newCell.
		 */
		Cell newCell;
		for (int i=1; i<N; i++) {

			boolean cellEmpty = false;		//Assume initially that the cell is occupied
			int numberOfTries = 0;			//Keep track of tries. If too many, we're stuck!

			while(cellEmpty == false) {

				//Generate next cell in chain
				//newCell = Cell.randomAdjacentCell(poly[i-1].getPos(), rndm);
				
				conf = EndConfiguration.ENDMONOMER;
				Map[] relativeMoves = conf.getPossibleMoves();
				int random = (int) (rndm.nextDouble()*6);
				newCell = Cell.Add(relativeMoves[random], poly[i-1].getPos());
				

				//Check if proposed new cell is empty
				if (grid.getCell(newCell)==-1) {
					cellEmpty = true;
					poly[i] = new Monomer(newCell, box);
					grid.update(newCell, i);
				}
				numberOfTries ++;

				/*
				 * If too many tries go by, it means the initialisation
				 * is stuck. Solution: go 20% back and try again.
				 */
				if (numberOfTries > 15) {
					int temp = i;
					i *= 0.8;		//Set i 20% back

					//Remove already added monomers from grid
					for (; temp>(i*0.8) ; temp--) {
						grid.update(poly[temp-1].getPos(), -1);
					}
					cellEmpty = true;	//Needs to be here. Think about it
				}	
			}	
		}
		System.out.println("Initialisation finished.");
	}

	/*
	 * Update polymer numOfSweeps times. One sweep corresponds to
	 * trying to update the polymer as many times as its length.
	 */
	public void update(int numOfSweeps) {
		for (int i=0; i<numOfSweeps; i++) {
			for (int j=0; j<N; j++) {
				singleUpdate();
			}
		}
	}

	//Update only a single monomer in chain
	public void singleUpdate() {

		//Find random monomer in chain
		int index = (int) (rndm.nextDouble()*N);

		//Case 1: Monomer is at the end of chain
		if (index == 0 || index == (N-1)) {
			updateEndMonomer(index);
		}

		//Case 2: Monomer is somewhere in the middle of the chain
		else { updateInternalMonomer(index); }
	}

	/*
	 * Update monomer at end of chain.
	 * End monomers can only undergo rotation.
	 */
	protected void updateEndMonomer(int index) {
		/*
		 * Set problem up. Either for first monomer in chain
		 * or very last monomer in chain.
		 */
		Cell nearest; //Nearest monomer in chain
		if (index == 0) { nearest = poly[1].getPos(); }
		else { nearest = poly[N-2].getPos(); }

		//Import the configuration and produce a set of relative moves
		conf = EndConfiguration.ENDMONOMER;
		Map[] relativeMoves = conf.getPossibleMoves();

		/*
		 * Probability of either move in configuration is
		 * 1/6. Choose one at random. Add the relative move to the
		 * position of nearest to get the new move of the end
		 * monomer. Try the move: If cell is empty, perform move.
		 */
		int random = (int) (rndm.nextDouble()*6);
		Cell newMove = Cell.Add(relativeMoves[random], nearest);
		tryMove(newMove, poly[index].getPos(), index);
	}

	/*
	 * Update monomer situated in the middle of the chain.
	 * Internal monomer can undergo kink and crankshaft moves.
	 */
	protected void updateInternalMonomer(int index) {
		/*
		 * Choose to perform either kink move, crankshaft move
		 * with previous monomer, or crankshaft move with next monomer
		 * with probability 1/3 for each.
		 */
		int random = (int) (rndm.nextDouble()*3);
		if (random == 0) { kinkMove(index); }
		else if (random == 1) { crankPrev(index); }
		else { crankNext(index); }
	}

	/*
	 * Method for performing a kink move.
	 */
	protected void kinkMove(int index) {
		/*
		 * Check if kink move is possible: By a geometrical argument
		 * a kink move is impossible if distance between next and
		 * previous monomer in any direction is > sqrt(2).
		 */
		Map nextMinusPrev = Map.getRelativeMap(poly[index-1].getPos(), 
								poly[index+1].getPos());		//Gets relative "vector" between prev and next
		boolean kink = true;
		if (nextMinusPrev.getDist() > 1.5) {	//sqrt(2) = 1.4
			kink = false;			//Kink move not possible
		}

		/*
		 * By a geometrical argument, the relative kink move (for the
		 * given index) is given by delta = (prev - current) + (next - current).
		 * The new move is then old position + delta.
		 */
		if (kink == true) {
			Map prevMinusCurrent = Map.getRelativeMap(poly[index].getPos(),poly[index-1].getPos());	//relative(a,b) does b-a
			Map nextMinusCurrent = Map.getRelativeMap(poly[index].getPos(),poly[index+1].getPos());
			Cell current = poly[index].getPos();
			Cell newMove = Cell.Add(prevMinusCurrent, nextMinusCurrent, current);
			tryMove(newMove, current, index);
		}
	}

	/*
	 * Method for performing a crankmove with previous monomer in chain.
	 */
	protected void crankPrev(int index) {
		/*
		 * Check if index-2, index-1, index, index+1 are in same plane.
		 * This is a simple initial way of dismissing a lot incorrect configurations.
		 */
		int plane = -1;
		if (index >=3 && index <= N-2) {
			plane = Cell.checkSamePlane(poly[index-2].getPos(),
					poly[index-1].getPos(),poly[index].getPos(),
					poly[index+1].getPos());
		}

		//Only move on if in same plane
		if (plane != -1) {
			boolean rightConfiguration = false;	//Assume we are in the wrong configuration to do crank move
			Map[] map = Map.getRelativeMap(poly[index-2].getPos(),poly[index-1].getPos(),
					poly[index].getPos(),poly[index+1].getPos());

			//XY plane
			if (plane==2) {

				//Check if the map corresponds to a crank configuration
				for (int i=0; i<8; i++) {
					if (Map.compareMaps(map, cranksXY[i]) == true) {
						conf = CrankXYConfs[i];
						rightConfiguration = true; break;
					}
				}
			}

			//XZ plane
			else if (plane==1) {

				//Check if the map corresponds to a crank configuration
				for (int i=0; i<8; i++) {
					if (Map.compareMaps(map, cranksXZ[i]) == true) {
						conf = CrankXZConfs[i];
						rightConfiguration = true; break;
					}
				}
			}

			//YZ plane
			else {

				//Check if the map corresponds to a crank configuration
				for (int i=0; i<8; i++) {
					if (Map.compareMaps(map, cranksYZ[i]) == true) {
						conf = CrankYZConfs[i];
						rightConfiguration = true; break;
					}
				}	
			}

			//If in the right configuration for a crankshaft move
			if (rightConfiguration == true) {
				//Positions of current and prev goes into array
				Cell current = poly[index].getPos();
				Cell prev = poly[index-1].getPos();
				Cell[] oldPositions = {prev, current};

				//Get possible moves and generate random number
				Map[] possibleMoves = conf.getPossibleMoves();
				int random = (int) (rndm.nextDouble()*3);

				//Create the new moves and put them in array
				Cell newMoveCurrent = Cell.Add(possibleMoves[random], current);
				Cell newMovePrev = Cell.Add(possibleMoves[random], prev);
				Cell[] newMoves = {newMovePrev, newMoveCurrent};

				//Create array of indices
				int[] indices = new int[] {index-1, index};

				//Try and perform the moves
				tryTwoMoves(newMoves, oldPositions, indices);
			}
		}
	}

	/*
	 * Method for performing a crankmove with next monomer in chain.
	 */
	protected void crankNext(int index) {

		//Check if index-1, index, index+1, index+2 are in same plane
		int plane = -1;
		if (index >=2 && index <= N-3) {
			plane = Cell.checkSamePlane(poly[index-1].getPos(),
					poly[index].getPos(),poly[index+1].getPos(),
					poly[index+2].getPos());
		}

		//Only move on if in same level
		if (plane != -1) {
			boolean rightConfiguration = false;	//Assume we are in the wrong configuration to do crank move
			Map[] map = Map.getRelativeMap(poly[index-1].getPos(),poly[index].getPos(),
					poly[index+1].getPos(),poly[index+2].getPos());

			//XY plane
			if (plane==2) {

				//Check if the map corresponds to a crank configuration
				for (int i=0; i<8; i++) {
					if (Map.compareMaps(map, cranksXY[i]) == true) {
						conf = CrankXYConfs[i];
						rightConfiguration = true; break;
					}
				}
			}

			//XZ plane
			else if (plane==1) {

				//Check if the map corresponds to a crank configuration
				for (int i=0; i<8; i++) {
					if (Map.compareMaps(map, cranksXZ[i]) == true) {
						conf = CrankXZConfs[i];
						rightConfiguration = true; break;
					}
				}
			}

			//YZ plane
			else {

				//Check if the map corresponds to a crank configuration
				for (int i=0; i<8; i++) {
					if (Map.compareMaps(map, cranksYZ[i]) == true) {
						conf = CrankYZConfs[i];
						rightConfiguration = true; break;
					}
				}	
			}

			//If in the right configuration for crankshaft
			if (rightConfiguration == true) {
				//Positions of current and prev goes into array
				Cell current = poly[index].getPos();
				Cell next = poly[index+1].getPos();
				Cell[] oldPositions = {current, next};

				//Get possible moves and generate random number
				Map[] possibleMoves = conf.getPossibleMoves();
				int random = (int) (rndm.nextDouble()*3);

				//Create the new moves and put them in array
				Cell newMoveCurrent = Cell.Add(possibleMoves[random], current);
				Cell newMoveNext = Cell.Add(possibleMoves[random], next);
				Cell[] newMoves = {newMoveCurrent, newMoveNext};

				//Create array of indices
				int[] indices = new int[] {index, index+1};

				//Try and perform the moves
				tryTwoMoves(newMoves, oldPositions, indices);
			}
		}
	}

	/*
	 * Method for trying to move a monomer to a newCell.
	 * The monomer of a certain index is undergoing a move from an oldCell
	 * to a newCell. The move is allowed if the newCell isn't already occupied.
	 */
	protected void tryMove(Cell newCell, Cell oldCell, int index) {
		if (grid.getCell(newCell) == -1) {

				poly[index].setPos(newCell);
				grid.update(oldCell, -1);
				grid.update(newCell, index);
		}
		//Otherwise move is not possible.
	}

	/*
	 * Method for trying to move two monomers to newCells.
	 * As for tryMove, only now with arrays of cells and indices.
	 */
	protected void tryTwoMoves(Cell[] newCells, Cell[]oldCells, int[] indices) {
		if (grid.getCell(newCells[0]) == -1 && grid.getCell(newCells[1]) == -1) {
			tryMove(newCells[0], oldCells[0], indices[0]);	//Theoretically, this checks if newCells are zero twice.
			tryMove(newCells[1], oldCells[1], indices[1]);	//Could save two comparisons by writing additional code
		}
	}

	//toVMD
	public String toVMD() {
		String output = "";
		for (int i=0; i<N; i++) {
			output += "Monomer" + i + " " + poly[i].printPos() + "\n";
		}
		return output;
	}
	
	public Monomer[] getPoly() { return poly; }
	public Monomer getPoly(int i) { return poly[i]; }
	public Random getRandom() { return rndm; }
	public int getBox() { return box; }
	public int getDNALength() { return N; }
}