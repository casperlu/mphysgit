package cl.DNA;

import java.util.Random;

import cl.AffinityProtein.AffinityManager;
import cl.AffinityProtein.ManyAffProteins;
import cl.Lattice.Cell;

public class PolymerManyAffProteins extends PolymerOneProtein {

	//Internal variables
	private ManyAffProteins prots;

	public PolymerManyAffProteins(int boxLength, int DNALength, Random r) {
		super(boxLength, DNALength, r);
		
		/*
		 * Make empty ManyProteins instance.
		 * This allows the polymer to perform an initial update
		 * so as to get into equilibrium.
		 * 
		 * This ManyProtein instance must be updated using the
		 * setProteins method!!!
		 */
		AffinityManager emptyAffMan = null;
		prots = new ManyAffProteins(0, boxLength, this, r, 1, emptyAffMan);
	}

	/*
	 * Overrides so as to define trymove for a polymer with many proteins.
	 * Takes into account the fact that you can't move a polymer with a protein
	 * into another protein.
	 */
	@Override
	protected void tryMove(Cell newCell, Cell oldCell, int index) {
		if (grid.getCell(newCell) == -1) {

			poly[index].setPos(newCell);
			grid.update(oldCell, -1);
			grid.update(newCell, index);

			//In case there is a protein on the monomer, move it with the monomer
			if (poly[index].getOccupation() != -1) {

				//Make sure new position doesn't already have a protein in it
				if (prots.getProtGrid(newCell) == -1) {
					int occupation = poly[index].getOccupation();
					prots.doMove(occupation, newCell);	//Updates both position and protein grid
				}

				//If it does, undo polymer move (happens very rarely)
				else {
					poly[index].setPos(oldCell);
					grid.update(newCell, -1);
					grid.update(oldCell, index);
				}
			}
		}
		//Otherwise move is not possible.
	}

	/*
	 * Overrides tryTwoMoves from polymer. Takes into account the fact that you
	 * can't move a polymer with a protein into another protein.
	 */
	@Override
	protected void tryTwoMoves(Cell[] newCells, Cell[]oldCells, int[] indices) {

		//No proteins to move
		if (grid.getCell(newCells[0]) == -1 && grid.getCell(newCells[1]) == -1
				&& prots.getProtGrid(oldCells[0]) == -1 && prots.getProtGrid(oldCells[1]) == -1) {
			tryMove(newCells[0], oldCells[0], indices[0]);	//Theoretically, this checks if newCells are zero twice.
			tryMove(newCells[1], oldCells[1], indices[1]);	//Could save two comparisons by writing additional code
		}

		//One protein to move
		else if (grid.getCell(newCells[0]) == -1 && grid.getCell(newCells[1]) == -1
				&& prots.getProtGrid(oldCells[0]) != -1 && prots.getProtGrid(oldCells[1]) == -1) {

			//Check if new protein position is free from proteins
			if (prots.getProtGrid(newCells[0]) == -1) {
				tryMove(newCells[0], oldCells[0], indices[0]);	//Theoretically, this checks if newCells are zero twice.
				tryMove(newCells[1], oldCells[1], indices[1]);	//Could save two comparisons by writing additional code
			}
		}

		//One protein to move
		else if (grid.getCell(newCells[0]) == -1 && grid.getCell(newCells[1]) == -1
				&& prots.getProtGrid(oldCells[0]) == -1 && prots.getProtGrid(oldCells[1]) != -1) {

			//Check if new protein position is free from proteins
			if (prots.getProtGrid(newCells[1]) == -1) {
				tryMove(newCells[0], oldCells[0], indices[0]);	//Theoretically, this checks if newCells are zero twice.
				tryMove(newCells[1], oldCells[1], indices[1]);	//Could save two comparisons by writing additional code
			}
		}

		//Two proteins to move
		else if (grid.getCell(newCells[0]) == -1 && grid.getCell(newCells[1]) == -1
				&& prots.getProtGrid(oldCells[0]) != -1 && prots.getProtGrid(oldCells[1]) != -1) {

			//Check if new protein positions are free from proteins
			if (prots.getProtGrid(newCells[0]) == -1 && prots.getProtGrid(newCells[1]) == -1) {
				tryMove(newCells[0], oldCells[0], indices[0]);	//Theoretically, this checks if newCells are zero twice.
				tryMove(newCells[1], oldCells[1], indices[1]);	//Could save two comparisons by writing additional code
			}
		}
	}

	//Setter to supply array of proteins
	public void setProteins(ManyAffProteins p) { prots = p; }
}