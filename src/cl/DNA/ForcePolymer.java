package cl.DNA;

import java.util.Random;

import cl.Lattice.Cell;

/*
 * ForcePolymer is a subclass of polymer. It describes a polymer
 * under a stretching force in the z-direction.
 */
public class ForcePolymer extends Polymer {

	//Internal variables
	private double force = 5;	//By default set to 5

	/*
	 * Constructor calls super.
	 */
	public ForcePolymer(int boxLength, int DNALength, Random rndm) {
		super(boxLength, DNALength, rndm);
	}

	/*
	 * Overrides tryMove for a polymer with a force in z-direction.
	 * The force only really affects the behaviour of the end monomers.
	 * Here the metropolis part takes place. DeltaE for a move is computed.
	 * 
	 * If less than zero move is always performed. If bigger than zero, then
	 * it is performed with probability.
	 */
	@Override
	protected void tryMove(Cell newCell, Cell oldCell, int index) {
		//This is commented out so that DNA may overlap - for faster equilibrium times
		//if (grid.getCell(newCell) == 0) {

		if (grid.getCell(newCell) == -1) {

			boolean performUpdate = true;

			/*
			 * Metropolis part. Only necessary for endmonomers
			 */
			if (index == 0 || index == N-1) {

				//Compute deltaE of move
				double deltaE;
				if (index == 0) {
					deltaE = -force*(poly[N-1].getPos().getCoord(2) - newCell.getCoord(2))
							+ force*(poly[N-1].getPos().getCoord(2) - oldCell.getCoord(2));
				}
				else { deltaE = -force*(newCell.getCoord(2) - poly[0].getPos().getCoord(2))
						+ force*(oldCell.getCoord(2) - poly[0].getPos().getCoord(2));
				}

				if (deltaE > 0) {
					double random = rndm.nextDouble();
					if (random > Math.exp(-deltaE)) {
						performUpdate = false;
					}
				}
			}

			if (performUpdate == true) {
				poly[index].setPos(newCell);
				grid.update(oldCell, -1);
				grid.update(newCell, index);
			}
		}
	}

	//Otherwise move is not possible.
	//}

	public void setForce(double f) {
		force = f;
	}

	/*
	 * Compute end to end extension
	 * THIS DOES NOT TAKE INTO ACCOUNT BOUNDARY CONDITIONS. THIS IS A HUGE
	 * PROBLEM. QUICK FIX: INITIALISE POLYMER AT MIDDLE OF BOX.
	 */
	public int extension() {
		return poly[N-1].getPos().getCoord(2) - poly[0].getPos().getCoord(2);
	}
}
