package cl.LacITests;

import java.io.Console;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;
import cl.DNA.PolymerManyAffProteins;
import cl.GenomeReader.GenomeManagerPWM;
import cl.PWM.PWM;
import cl.PWM.PWMProducer;
import cl.ProteinDNASystem.ManyAffProteinsDNASystem;

public class RandomAllSearch {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		Console c = System.console();
		Random rndm;
		int length = 1000;
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		int numOfSeeds = Integer.parseInt(c.readLine("Type in number of seeds: "));
		double[] targetTimes = new double[numOfSeeds];
		double DNAAff = Double.parseDouble(c.readLine("Type in initial DNA affinity: "));
		double finalDNAAff = Double.parseDouble(c.readLine("Type in final (not included) DNA affinity: "));

		PolymerManyAffProteins p;
		ManyAffProteinsDNASystem s;

		//Set up motifs and feed them to producer
		String[] bindingMotifsOfLacI = new String[] {"AAATGTNNNNNNNNNACAACC", "AATTGTNNNNNNNNNACAATT", "AATTGCNNNNNNNNNACTGCC"}; //"GGCAGTNNNNNNNNNGCAATT"}; // "AATTGCNNNNNNNNNACTGCC"};
		PWMProducer producer = new PWMProducer(bindingMotifsOfLacI);

		//Compute PWM. Takes epsilonStar as argument
		PWM pwm = producer.computePWM(2);

		String filePath = "../Data/Genomes/Ecoli_K12_MG1655.fna"; //Ecoli_K12_DH10B.fna"; //chr1.fa";
		GenomeManagerPWM gm = new GenomeManagerPWM(pwm);
		gm.ReadInGenome(filePath);

		double[] affinities = gm.computeAffinities("AATTGTTATCCGCTCACAATT", length, targetPos, "Highest");

		//Binding sites positions in monomer units
		int[] bindingSitePositions = {480,500,505}; //O2,O1,O3
		
		//Define output file for simulation
		String outputName = c.readLine("Type special output name to make it clear that advanced settings " +
				"have been chosen: ");
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/AffTestsForAffProt/RealProt/" + outputName));

		/*
		 * Run simulations for given affinity range
		 */
		for (; DNAAff<finalDNAAff; DNAAff+=1.0) {

			//Run for a given number of seeds
			for (int seed=0; seed<numOfSeeds; seed++) {
				
				//Set random number generator
				rndm = new Random(seed);
				p = new PolymerManyAffProteins(box,length,rndm);
				s = new ManyAffProteinsDNASystem(p, targetPos, box, DNAAff, affinities, 3);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				System.out.println("Search time: " + s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + DNAAff + " simulated.");
			}
			printer.println(DNAAff + "\t" + averageOfArray(targetTimes) + "\t" + computeError(targetTimes)
					+ "\t" + computeStandardErrorofMean(targetTimes));
			printer.flush();
		}

		printer.close();
	}

	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}

	private static double computeError(double[] array) {
		double mean = averageOfArray(array);
		double xMinusMeanSquared = 0;
		for (int i=0; i<array.length; i++) {
			System.out.println(array[i]);
			xMinusMeanSquared += (array[i] - mean)*(array[i] - mean);
		}
		return Math.sqrt(xMinusMeanSquared/(array.length));
	}

	private static double computeStandardErrorofMean(double[] array) {
		double stDev = computeError(array);
		return stDev/(Math.sqrt(array.length));
	}
}