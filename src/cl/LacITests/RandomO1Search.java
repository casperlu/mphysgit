package cl.LacITests;

import java.io.Console;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.GenomeReader.GenomeManagerPWM;
import cl.PWM.PWM;
import cl.PWM.PWMProducer;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class RandomO1Search {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		Console c = System.console();
		Random rndm;
		int length = Integer.parseInt(c.readLine("Type in DNA length: "));
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		int numOfSeeds = Integer.parseInt(c.readLine("Type in number of seeds: "));
		double[] targetTimes = new double[numOfSeeds];
		double[] onDNATimes = new double[numOfSeeds];
		double DNAAff = Double.parseDouble(c.readLine("Type in initial DNA affinity: "));
		double finalDNAAff = Double.parseDouble(c.readLine("Type in final (not included) DNA affinity: "));

		PolymerOneProtein p;
		RandomProteinDNASystem s;

		//Set up motifs and feed them to producer
		String[] bindingMotifsOfLacI = new String[] {"AAATGTNNNNNNNNNACAACC", "AATTGTNNNNNNNNNACAATT", "AATTGCNNNNNNNNNACTGCC"}; //"GGCAGTNNNNNNNNNGCAATT"}; // "AATTGCNNNNNNNNNACTGCC"};
		PWMProducer producer = new PWMProducer(bindingMotifsOfLacI);

		//Compute PWM. Takes epsilonStar as argument
		PWM pwm = producer.computePWM(2);

		String filePath = "../Data/Genomes/Ecoli_K12_MG1655.fna"; //Ecoli_K12_DH10B.fna"; //chr1.fa";
		GenomeManagerPWM gm = new GenomeManagerPWM(pwm);
		gm.ReadInGenome(filePath);

		double[] affinities = gm.computeAffinities("AATTGTTATCCGCTCACAATT", length, targetPos, "Highest");

		//Define output file for simulation
		String outputName = c.readLine("Type special output name to make it clear that advanced settings " +
				"have been chosen: ");
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/AffTestsForAffProt/RealProt/" + outputName));

		/*
		 * Run simulations for given affinity range
		 */
		for (; DNAAff<finalDNAAff; DNAAff+=1.0) {

			//Run for a given number of seeds
			for (int seed=0; seed<numOfSeeds; seed++) {
				
				//Set random number generator
				rndm = new Random(seed);
				p = new PolymerOneProtein(box,length,rndm);
				s = new RandomProteinDNASystem(p, targetPos, box, DNAAff, affinities);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				onDNATimes[seed] = s.getOnDNATime();
				double percentOfTimeOnDNA = (100.0*s.getOnDNATime()/((double)(s.getTime())));
				System.out.println("Search time: " + s.getTime() + "\tTime spent on DNA: " + s.getOnDNATime()
						+ " and in percent: " + percentOfTimeOnDNA);
				System.out.println("Seed " + seed + "\tAffinity " + DNAAff + " simulated.");
			}
			printer.println(DNAAff + "\t" + averageOfArray(targetTimes) + "\t" + computeError(targetTimes)
					+ "\t" + computeStandardErrorofMean(targetTimes) + "\t" + averageOfArray(onDNATimes)
					+ "\t" + computeError(onDNATimes)+ "\t" + computeStandardErrorofMean(onDNATimes));
			printer.flush();
		}

		printer.close();
	}

	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}

	private static double computeError(double[] array) {
		double mean = averageOfArray(array);
		double xMinusMeanSquared = 0;
		for (int i=0; i<array.length; i++) {
			System.out.println(array[i]);
			xMinusMeanSquared += (array[i] - mean)*(array[i] - mean);
		}
		return Math.sqrt(xMinusMeanSquared/(array.length));
	}

	private static double computeStandardErrorofMean(double[] array) {
		double stDev = computeError(array);
		return stDev/(Math.sqrt(array.length));
	}
}