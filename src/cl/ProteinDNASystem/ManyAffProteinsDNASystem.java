package cl.ProteinDNASystem;

import java.util.Random;

import cl.AffinityProtein.AffinityManager;
import cl.AffinityProtein.ManyAffProteins;
import cl.DNA.PolymerManyAffProteins;

public class ManyAffProteinsDNASystem extends DNASystem {

	//Internal variables
	protected ManyAffProteins prots;
	protected Random rndm;
	protected int target, box, N, typeOfProtein;
	protected double aff;
	private AffinityManager AM;

	public ManyAffProteinsDNASystem(PolymerManyAffProteins p, int targetPosition, int box, double DNAAff, 
			double[] affinities,int  numProt) {
		super(p);	//NEEDS CHANGED
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = DNAAff;
		AM = new AffinityManager(DNAAff, target, affinities);
		N = numProt;
		typeOfProtein = 0;
	}

	public ManyAffProteinsDNASystem(PolymerManyAffProteins p, int targetPosition, int box, double DNAAff, 
			double[] affinities, int distFromTarget,int  numProt) {
		super(p);	//NEEDS CHANGED
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = DNAAff;
		AM = new AffinityManager(DNAAff, target, affinities);
		N = numProt;
		typeOfProtein = 1;
	}

	@Override
	public void initialise() {
		poly.initialise(-1, -1, -1);
		((PolymerManyAffProteins) poly).setTarget(target, 1);	//Target value = 1
		poly.update(25000);		//Equilibration time

		//AffProtein at random
		if (typeOfProtein == 0) {
			prots = new ManyAffProteins(N, box, (PolymerManyAffProteins) poly, poly.getRandom(), target, AM);	//N proteins
		}
		((PolymerManyAffProteins) poly).setProteins(prots);
		time = 0;
	}

	@Override
	public void update() {
		poly.update(1);
		prots.update();
		time ++;
	}

	public boolean getTargetStatus() { return prots.getTargetStatus(); }

	public String toVMD() {
		String sPoly = poly.toVMD();
		String sProt = prots.toVMD();
		return sPoly + sProt;
	}
}