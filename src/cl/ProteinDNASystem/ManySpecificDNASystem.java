package cl.ProteinDNASystem;

import java.util.Random;
import cl.DNA.PolymerManyProteins;
import cl.Protein.ManyProteins;

public class ManySpecificDNASystem {

	//Internal variables
		protected PolymerManyProteins poly;
		protected int time;
		protected ManyProteins prots;
		protected Random rndm;
		protected int target, box, onDNAtime;
		protected double aff;
		protected int NProt, distFromTarget, protPos;
		
		public ManySpecificDNASystem(PolymerManyProteins p, int targetPosition, int box, double affinity, 
										int numOfProteins, int distanceFromTarget) {
			poly = p;
			rndm = p.getRandom();
			target = targetPosition;
			this.box = box;
			aff = affinity;
			NProt = numOfProteins;
			distFromTarget = distanceFromTarget;
			protPos = target - distFromTarget;
		}
		
		public void initialise() {
			poly.initialise(-1, -1, -1);
			poly.setTarget(target, 1);	//Target value = 1;
			poly.update(25000);		//Equilibration time
			prots = new ManyProteins(NProt, box, poly, poly.getRandom(), aff, 1);
			poly.setProteins(prots);
			time = 0;
			
			//Initialise proteins evenly around targetposition
			int minus = protPos - NProt/2;	//Place first protein at minus
			int plus = protPos + NProt/2;	//Place last protein at plus
			
			//If NProt even
			if (NProt%2 == 0) {
				plus = protPos + NProt/2 - 1; //Think about it
			}
			
			/*
			 * Update protein positions so that they are on DNA
			 */
			for (int j=0, i=minus; i<= plus; i++, j++) {

				//Remove protein from protein grid
				prots.updateProtGrid(prots.getProt(j).getPos(), -1);	
				
				//Give protein new position
				prots.getProt(j).setPosition(poly.getPoly(i).getPos());
				
				//Add protein to protein grid
				prots.updateProtGrid(prots.getProt(j).getPos(), j);
				poly.updateOccupation(i, j); //Tell monomer i that it has a protein of index j on it
			}
		}
		
		public void update() {
			poly.update(1);
			prots.update();
			time ++;
		}
		
		public boolean getTargetStatus() { return prots.getTargetStatus(); }
		
		public int getTime() { return time; }
		
		public String toVMD() {
			String sPoly = poly.toVMD();
			String sProt = prots.toVMD();
			return sPoly + sProt;
		}
}

