package cl.ProteinDNASystem;

import java.util.Random;

import cl.AffinityProtein.AffProtein;
import cl.AffinityProtein.AffinityManager;
import cl.DNA.Polymer;
import cl.DNA.PolymerOneProtein;
import cl.Protein.Protein;

/*
 * THIS IS OLD CODE. IN THIS CODE THE PROTEIN KNOWS ABOUT THE
 * DNA, BUT NOT THE OTHER WAY AROUND.
 */
public class SimpleSystem {
	
	//Internal variables
	private PolymerOneProtein poly;
	private Protein prot;
	private Random rndm;
	private int time, onDNAtime;
	
	public SimpleSystem(int box, int DNALength, int targetPosition, long seed, double affinity) {
		rndm = new Random(seed);
		poly = new PolymerOneProtein(box, DNALength, rndm);
		poly.initialise(-1,-1,-1);		//Initialise at random position
		poly.setTarget(targetPosition, 1);	//Target value = 1
		prot = new Protein(box, poly, rndm, affinity,1);
		poly.setProtein(prot);
	}
	
	//Constructor for AffinityProtein
	public SimpleSystem(int box, int DNALength, int targetPosition, long seed, double DNAAffinity,
			double mean, double stDev, double targetAff, double cutOffAff, long AMSeed) {
		rndm = new Random(seed);
		poly = new PolymerOneProtein(box, DNALength, rndm);
		poly.initialise(-1,-1,-1);		//Initialise at random position
		poly.setTarget(targetPosition, 1);	//Target value = 1
		AffinityManager AM = new AffinityManager(DNALength, DNAAffinity, mean, stDev, targetPosition, targetAff, cutOffAff, AMSeed);
		prot = new AffProtein(box, poly, rndm, 1, AM);
		poly.setProtein(prot);
	}
	
	public void update() {
		poly.update(1);
		prot.update();
		time ++;
		onDNAtime += prot.getOnDNA();
		//System.out.println("Total time: " + time + " Time spent on DNA: " + onDNAtime);
	}

	public boolean getTargetStatus() { return prot.getTargetStatus(); }

	public String toVMD() {
		String sPoly = poly.toVMD();
		String sProt = prot.toVMD();
		return sPoly + sProt;
	}
	
	public Protein getProt() { return prot; }
	public Polymer getPoly() { return poly; }
}
