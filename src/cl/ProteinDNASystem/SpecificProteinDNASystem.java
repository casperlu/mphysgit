package cl.ProteinDNASystem;

import java.util.Random;

import cl.AffinityProtein.AffProtein;
import cl.AffinityProtein.AffinityManager;
import cl.DNA.PolymerOneProtein;
import cl.Lattice.Cell;
import cl.Protein.Protein;

public class SpecificProteinDNASystem extends DNASystem {

	//Internal variables
	protected Protein prot;
	protected Random rndm;
	protected int target, box, onDNAtime, initialProteinPosition;
	protected double aff;
	private int kindOfProtein;
	private AffinityManager AM;

	// Constructor for normal protein
	public SpecificProteinDNASystem(PolymerOneProtein p, int targetPosition, int box, double affinity, int distanceFromTarget) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = affinity;
		initialProteinPosition = target - distanceFromTarget;
		kindOfProtein = 0;
	}

	/*
	 * Constructor for AffProtein with Gaussian distribution
	 */
	public SpecificProteinDNASystem(PolymerOneProtein p, int targetPosition, int box, double DNAAff, int distanceFromTarget,
			double mean, double stDev, double targetAff, double cutOffAff, long AMSeed) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = DNAAff;
		initialProteinPosition = target - distanceFromTarget;
		AM = new AffinityManager(p.getDNALength(), DNAAff, mean, stDev, target, targetAff, cutOffAff, AMSeed);
		kindOfProtein = 1;
	}

	/*
	 * Constructor for AffProtein with real sequence. Protein produced in sphere around distanceFromTarget
	 */
	public SpecificProteinDNASystem(PolymerOneProtein p, int targetPosition, int box, double DNAAff,
			double[] affinities, int distanceFromTarget) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = DNAAff;
		initialProteinPosition = target + distanceFromTarget;	//The index of the monomer the gene ends at
		AM = new AffinityManager(DNAAff, target, affinities);
		kindOfProtein = 2;
	}

	@Override
	public void initialise() {
		poly.initialise(-1, -1, -1);
		((PolymerOneProtein) poly).setTarget(target, 1);	//Target value = 1
		poly.update(25000);		//Equilibration time

		// Normal protein
		if (kindOfProtein == 0) {
			prot = new Protein(box, poly.getPoly(initialProteinPosition).getPos(), (PolymerOneProtein) poly, poly.getRandom(), aff, 1);
		}

		// Protein with energy profile
		else if (kindOfProtein == 1) {
			prot = new AffProtein(box, poly.getPoly(initialProteinPosition).getPos(), (PolymerOneProtein) poly, poly.getRandom(), 1, AM);
		}

		// Protein with energy profile with production of protein in sphere
		else if (kindOfProtein == 2) {
			prot = new AffProtein(box, Cell.getRandomNeighbour(poly.getPoly(initialProteinPosition).getPos(), poly.getRandom()),
					(PolymerOneProtein) poly, poly.getRandom(), 1, AM);
		}

		((PolymerOneProtein) poly).setProtein(prot);
		((PolymerOneProtein) poly).updateOccupation(initialProteinPosition, 1);	//Tell DNA it has a protein on it
		time = 0;
		onDNAtime = 0;
	}

	@Override
	public void update() {
		poly.update(1);
		prot.update();
		time ++;
		onDNAtime += prot.getOnDNA();
	}

	public boolean getTargetStatus() { return prot.getTargetStatus(); }

	public String toVMD() {
		String sPoly = poly.toVMD();
		String sProt = prot.toVMD();
		return sPoly + sProt;
	}
	

	public int getOnDNATime() { return onDNAtime; }
}
