package cl.ProteinDNASystem;

import java.util.Random;

import cl.DNA.PolymerManyProteins;
import cl.Protein.ManyProteins;

public class ManyProteinsDNASystem extends DNASystem {
	
	//Internal variables
	protected ManyProteins prots;
	protected Random rndm;
	protected int target, box, onDNAtime, N;
	protected double aff;

	public ManyProteinsDNASystem(PolymerManyProteins p, int targetPosition, int box, double affinity, int numProt) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = affinity;
		N = numProt;
	}
	
	@Override
	public void initialise() {
		poly.initialise(-1, -1, -1);
		((PolymerManyProteins) poly).setTarget(target, 1);	//Target value = 1
		poly.update(25000);		//Equilibration time
		prots = new ManyProteins(N, box, (PolymerManyProteins) poly, poly.getRandom(), aff, 1);	//N proteins
		((PolymerManyProteins) poly).setProteins(prots);
		time = 0;
	}
	
	@Override
	public void update() {
		poly.update(1);
		prots.update();
		time ++;
	}

	public boolean getTargetStatus() { return prots.getTargetStatus(); }

	public String toVMD() {
		String sPoly = poly.toVMD();
		String sProt = prots.toVMD();
		return sPoly + sProt;
	}
}
