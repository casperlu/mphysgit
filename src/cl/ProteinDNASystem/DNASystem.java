package cl.ProteinDNASystem;


import cl.DNA.Polymer;

public class DNASystem {
	
	//Internal variables
	protected Polymer poly;
	protected int time;
	
	public DNASystem(Polymer p) {
		poly = p;
	}
	
	public void initialise() {
		poly.initialise(-1, -1, -1);
		time = 0;
	}
	
	public void update() {
		poly.update(1);
		time++;
	}
	
	public int getTime() { return time; }
}
