package cl.ProteinDNASystem;

import cl.DNA.PolymerManyProteins;
import cl.Lattice.Cell;
import cl.Protein.ManyProteins;

public class OnOffSystem extends ManyProteinsDNASystem {

	//Internal variables
	protected int distFromTarget;
	protected Cell TFpos;
	protected double TFaff;
	
	public OnOffSystem(PolymerManyProteins p, int targetPosition, int box,
			int distanceFromTarget, double aff1, double aff2) {
		super(p, targetPosition, box, aff1, 2);	//Sets both proteins to have aff1
		distFromTarget = distanceFromTarget;
		TFaff = aff2;
	}

	@Override
	public void initialise() {
		poly.initialise(-1, -1, -1);
		((PolymerManyProteins) poly).setTarget(target, 1);	//Target value = 1
		poly.update(25000);		//Equilibration time
		prots = new ManyProteins(2, box, (PolymerManyProteins) poly, poly.getRandom(), aff, 1);	//2 proteins
		((PolymerManyProteins) poly).setProteins(prots);
		
		// Update TF protein position
		prots.updateProtGrid(prots.getProt(0).getPos(), -1);
		TFpos = poly.getPoly(target-distFromTarget).getPos();
		prots.getProt(0).setPosition(TFpos);
		prots.updateProtGrid(prots.getProt(0).getPos(), 0);
		((PolymerManyProteins) poly).updateOccupation(target-distFromTarget, 0); //Tell monomer it has TF on it
		
		//Update TF affinity
		prots.getProt(0).setAffinity(TFaff);
		
		
		time = 0;
	}
	
	public int getOnOff() {
		if (prots.getProt(0).getTargetStatus() == true) {
			return 0;	//TF got to target first
		}
		else if(prots.getProt(1).getTargetStatus() == true) {
			return 1; 	//Polymerase got to target first
		}
		else { return -1; /* Target not reached yet */ }
	}
}
