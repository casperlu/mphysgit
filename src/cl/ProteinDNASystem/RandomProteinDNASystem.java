package cl.ProteinDNASystem;

import java.util.Random;

import cl.AffinityProtein.AffProtein;
import cl.AffinityProtein.AffinityManager;
import cl.DNA.PolymerOneProtein;
import cl.Protein.Protein;

public class RandomProteinDNASystem extends DNASystem {
	
	//Internal variables
	protected Protein prot;
	protected Random rndm;
	protected int target, box, onDNAtime;
	protected double aff;
	private int kindOfProtein;
	private AffinityManager AM;

	public RandomProteinDNASystem(PolymerOneProtein p, int targetPosition, int box, double affinity) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = affinity;
		kindOfProtein = 0;
	}
	
	/*
	 * Constructor for AffProtein with generator from statistical properties
	 */
	public RandomProteinDNASystem(PolymerOneProtein p, int targetPosition, int box, double DNAAff,
			double mean, double stDev, double targetAff, double cutOffAff, long AMSeed) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = DNAAff;
		AM = new AffinityManager(p.getDNALength(), DNAAff, mean, stDev, target, targetAff, cutOffAff, AMSeed);
		kindOfProtein = 1;
	}
	
	/*
	 * Constructor for AffProt with specific array of affinities inputted
	 */
	public RandomProteinDNASystem(PolymerOneProtein p, int targetPosition, int box, double DNAAff, double[] affinities) {
		super(p);
		rndm = p.getRandom();
		target = targetPosition;
		this.box = box;
		aff = DNAAff;
		AM = new AffinityManager(DNAAff, target, affinities);
		kindOfProtein = 1;
	}
	
	@Override
	public void initialise() {
		
		
		poly.initialise(-1, -1, -1);
		((PolymerOneProtein) poly).setTarget(target, 1);	//Target value = 1
		poly.update(25000);		//Equilibration time
		
		//Normal protein
		if (kindOfProtein == 0) {
			prot = new Protein(box, (PolymerOneProtein) poly, poly.getRandom(), aff, 1);
		}
		
		//Protein with energy profile
		else if (kindOfProtein == 1) {
			prot = new AffProtein(box, (PolymerOneProtein) poly, poly.getRandom(), 1, AM);
		}
		
		((PolymerOneProtein) poly).setProtein(prot);
		time = 0;
		onDNAtime = 0;
	}
	
	@Override
	public void update() {
		poly.update(1);
		prot.update();
		time ++;
		onDNAtime += prot.getOnDNA();
	}
	
	public boolean getTargetStatus() { return prot.getTargetStatus(); }

	public String toVMD() {
		String sPoly = poly.toVMD();
		String sProt = prot.toVMD();
		return sPoly + sProt;
	}
	
	public int getOnDNATime() { return onDNAtime; }
}
