package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import cl.AffinityProtein.Frame;
import cl.ProteinDNASystem.SimpleSystem;

public class SimpleSystemTest {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {


		/*
		 * CASE 1: Protein has samme affinity all over DNA
		 */
		//		int box = 20;
		//		int length = 500;
		//		int targetPos = length/2;
		//		long seed = 10;
		//		double affinity = 5;
		//		SimpleSystem s = new SimpleSystem(box, length, targetPos, seed, affinity);


		/*
		 * CASE 2: Protein has specific affinities all over DNA
		 */
		int length = 1000;
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = 75;//length/2;
		double targetAff = 50.16;
		long seed = 10;
		long AMSeed = 2;
		double DNAAff = 4;
		double stDev = 6.86;
		double mean = 0.0;
		double cutOffAff = 33.5;
		SimpleSystem s = new SimpleSystem(box, length, targetPos, seed, DNAAff,
				mean, stDev, targetAff, cutOffAff, AMSeed);
		//Introduce a visualisation frame
		Frame f = new Frame(s);
		f.repaint();

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("Data/SimpleTests/SimpleAffProt.xyz"));

		int i = 0;
		while (s.getTargetStatus() == false) {

			synchronized(s) {
				s.update();
				f.repaint();
				if (s.getProt().getOnDNA() == 1) {
					System.out.println(s.getPoly().grid.getCell(s.getProt().getPos()));
				}
			}
			//Thread.sleep(30, 0);
//			if (i%1 == 0) {
//				printer.println("" + (length+1));
//				printer.println("Point = " + i);
//				printer.print(s.toVMD());
//			}
//			i++;
		}

		printer.close();

	}

}
