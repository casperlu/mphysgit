package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class Histogram {
	
	//Internal variables
	//private int binSize, numOfBins;

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Random rndm = new Random(3);
		int box = 36;
		int length = 500;
		int targetPos = length/2;
		double[] affinities = new double[] {6};//{2, 6 ,10};
		int numOfSeeds = 2000;
		int[] targetResults = new int[numOfSeeds];
		

		PolymerOneProtein p;
		RandomProteinDNASystem s;

		//Define output file for simulation
		PrintWriter printer = null;
		
		for (int i=0; i<affinities.length; i++) {
			
			//Set up printer
			printer = new PrintWriter(new FileWriter("HistogramAff" + affinities[i] + "Seeds" + numOfSeeds+ ".dat"));
			
			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerOneProtein(box,length,rndm);
				s = new RandomProteinDNASystem(p, targetPos, box, affinities[i]);
				s.initialise();
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetResults[seed] = s.getTime();
				printer.println(s.getTime());
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + affinities[i] + " simulated.");
			}

			printer.close();
		}

	}
	
	
	/*
	 * We don't actually need this kind of stuff. Gnuplot does it for us!
	 */
	
//	/*
//	 * Constructor for histogram.
//	 */
//	public Histogram(int binSize) {
//		
//		this.binSize = binSize;
//	}
//	
//	public int[][] getHistogram(int[] data) {
//
//		numOfBins = computeNumOfBins(data);
//		int[][] hist = new int[numOfBins][2];
//		int index;
//		
//		for (int i=0; i<numOfBins; i++) { 
//			hist[i][0] = binSize*i;
//			hist[i][1] = 0;
//		}
//		
//		for (int i=0; i<data.length; i++) {
//			index = data[i]/binSize;		//e.g. for numOfBins=10 and data[i]=95 this gives index=95/10=9 which is last bin
//			
//		}
//		
//		return hist;
//	}
//	
//	private int computeNumOfBins(int[] data) {
//		//Run through data and find biggest value
//		int max = 0;
//		for (int i=0; i<data.length; i++) {
//			if (data[i] > max) { max = data[i]; }
//		}
//		
//		return (max/binSize) + 1;
//	}
//
//
//	private static double averageOfArray(double[] array) {
//		double value = 0;
//		for (int i=0; i<array.length; i++) {
//			value += array[i];
//		}
//		return value/(array.length);
//	}
}
