package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.Monomer;
import cl.DNA.Polymer;

/*
 * Test investigating the diffusion of the
 * center of mass.
 */
public class CenterOfMass {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		//System parameters
		int box = 500;
		int halfBox = box/2;
		int numberOfRuns = 100;
		int t = 10000;
		Random rndm;
		
		//Output file in the form log(D) \t log(N)
		PrintWriter printer = new PrintWriter(new FileWriter("CoM/CoM.dat"));

		//TODO: Boundary conditions probably screw things up!
		//QUICK FIX: INITIALISE ALL POLYMERS AT THE CENTRE OF THE BOX
		//Loop over number of particles in the simulations
		for (int N=50; N<500; N+=30) {
			double[] CoMs = new double[numberOfRuns];

			//Loop over different seeds
			for (int seed=0; seed<numberOfRuns; seed++) {
				rndm = new Random(seed);
				Polymer poly = new Polymer(box, N, rndm);
				poly.initialise(halfBox, halfBox, halfBox);
				
				//Get initial CoM
				double[] CoM0 = computeCoM(poly, box);
				poly.update(t);
				
				//Get CoM at time t
				double[] CoMt = computeCoM(poly, box);
				
				/*
				 * For each seed find (CoM(t)-CoM(0))^2
				 */
				CoMs[seed] = 0.0;
				for (int i=0; i<3; i++) {
					CoMs[seed] += (CoMt[i]-CoM0[i])*(CoMt[i]-CoM0[i]);
				}
			}
			
			/*
			 * Produce output: D is the seed average of CoMs divided by 6t.
			 * 
			 * Output is of the form log(N) \t log(D)
			 */
			printer.println(Math.log(N) +"\t" + Math.log(averageOfArray(CoMs)/(6.0*t)));
			System.out.println("Printing done for N = " + N);
		}

		printer.close();
	}

	/*
	 * Method for computing the center of mass.
	 * DOESN'T TAKE INTO ACCOUNT BOUNDARY CONDITIONS!
	 */
	static double[] computeCoM(Polymer poly, int box) {
		Monomer[] currentPoly = poly.getPoly();
		int N = currentPoly.length;
		double[] CoM = new double[] {0,0,0};
		int[] pos;
		for (int i=0; i<N; i++) {
			pos = currentPoly[i].getPos().getCoords();
			for (int j=0; j<3; j++) {
					CoM[j] += pos[j];
			}
		}
		for (int i=0; i<3; i++) {
			CoM[i] /= N;
		}
		return CoM;
	}

	/*
	 * Method for finding the average of an array
	 */
	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/((double) array.length);
	}
}
