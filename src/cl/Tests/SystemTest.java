package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class SystemTest {

	public static void main(String[] args) throws IOException {

		Random rndm = new Random(3);
		int box = 30;
		int length = 50;
		int targetPos = length/2;
		double affinity = 1;

		PolymerOneProtein p = new PolymerOneProtein(box,length,rndm);

		RandomProteinDNASystem s = new RandomProteinDNASystem(p, targetPos, box, affinity);

		s.initialise();

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("SystemTest.xyz"));

		int i = 0;
		while (s.getTargetStatus() == false) {
			s.update();
			//if (i%1 == 0) {
				printer.println("" + (length+1));
				printer.println("Point = " + i);
				printer.print(s.toVMD());
		//	}
			i++;
		}

		printer.close();
	}
}
