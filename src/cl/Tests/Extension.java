package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.ForcePolymer;

public class Extension {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		int box = 500;
		int halfBox = box/2;
		int length = 200;
		int numSeeds = 10;
		ForcePolymer poly;
		double[] extension = new double[200];
		double[][] seedExtensions = new double[numSeeds][50];
		Random rndm;
		
		PrintWriter printer2 = new PrintWriter(new FileWriter("NewExtensionTest.dat"));
		for (int seed = 0; seed<numSeeds; seed++) {
			rndm = new Random(seed);
			poly = new ForcePolymer(box, length, rndm);
			poly.initialise(halfBox,halfBox,halfBox);
			for (double force=0, j=0; force<9.85; force +=0.2, j++) {
				//poly = new Polymer(box, length);
				//poly.initialise(seed);
				poly.setForce(force);
				poly.update(80000);
				for (int i=1; i<200; i++) {
					poly.update(10);
					extension[i] = poly.extension();
				}
				seedExtensions[seed][(int)j] = averageOfArray(extension)/length;
				
				//printer2.println(averageOfArray(extension)/length + "\t" + force);
				System.out.println("Printing done for force " + force + " seed " + seed);
			}
		}
		System.out.println("Simulation finished.");
		
		for (double force=0, j=0; force <9.85; force +=0.2, j++) {
			double value = 0;
			for (int i=0; i<numSeeds; i++) {
				value += seedExtensions[i][(int)j];
			}
			value/= numSeeds;
			printer2.println(force + "\t" + value);
		}
		printer2.close();
	}

	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}

}
