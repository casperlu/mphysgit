package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.ForcePolymer;

/*
 * Simple test of polymer.
 */
public class Test {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		//System parameters
		int box = 500;
		int length = 200;
		Random rndm = new Random(5);
		ForcePolymer poly = new ForcePolymer(box, length, rndm);
		
		//Initialise polymer at a random position
		poly.initialise(-1,-1,-1);
		System.out.println(poly.toVMD());
		
		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("traj.xyz"));

		//Simulate for 20000 sweeps
		for (int i=1; i<20000; i++) {
			poly.update(1);
			printer.println("" + length);
			printer.println("Point = " + i);
			printer.print(poly.toVMD());
		}
		System.out.println("Simulation finished.");
		printer.close();
	}
}
