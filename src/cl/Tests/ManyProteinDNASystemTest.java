package cl.Tests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerManyProteins;
import cl.ProteinDNASystem.ManyProteinsDNASystem;

public class ManyProteinDNASystemTest {

	public static void main(String[] args) throws IOException {

		Random rndm = new Random(3);
		int box = 30;
		int length = 200;
		int targetPos = length/2;
		double affinity = 1;

		PolymerManyProteins p = new PolymerManyProteins(box,length,rndm);

		ManyProteinsDNASystem s = new ManyProteinsDNASystem(p, targetPos, box, affinity,10);

		s.initialise();

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("Data/ManyProtein/VMDTest.xyz"));

		int i = 0;
		while (s.getTargetStatus() == false) {
			s.update();
			//if (i%1 == 0) {
				printer.println("" + (length+10));
				printer.println("Point = " + i);
				printer.print(s.toVMD());
		//	}
			i++;
		}

		printer.close();
	}
}
