package cl.AffTestForAffProt;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class SimpleTestB {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Random rndm = new Random(3);
		int length = Integer.parseInt(args[0]);
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		int numOfSeeds = Integer.parseInt(args[1]);

		//Define the protein
		long AMSeed = Long.parseLong(args[2]);	//2
		double stDev = Double.parseDouble(args[3]);	//6.86;
		double mean = 0.0;
		double cutOffAff = Double.parseDouble(args[4]); // 33.5; 
		double targetAff = 50.16;
		String outputName = args[5];

		PolymerOneProtein p;
		RandomProteinDNASystem s;

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/AffTestsForAffProt/OneProt/" + outputName));
		double[] targetTimes = new double[numOfSeeds];
		
		
		for (double DNAAff=0.0; DNAAff<12; DNAAff+=1.0) {

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerOneProtein(box,length,rndm);
				s = new RandomProteinDNASystem(p, targetPos, box, DNAAff,
						mean, stDev, targetAff, cutOffAff, AMSeed);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + DNAAff + " simulated.");
			}
			printer.println(DNAAff + "\t" + averageOfArray(targetTimes));
			printer.flush();
		}

		printer.close();
	}

	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}
}