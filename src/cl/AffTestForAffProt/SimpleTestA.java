package cl.AffTestForAffProt;

import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class SimpleTestA {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Console c = System.console();
		Random rndm = new Random(3);
		int length = Integer.parseInt(c.readLine("Type in DNA length: "));
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		int numOfSeeds = Integer.parseInt(c.readLine("Type in number of seeds: "));

		//Define the protein
		long AMSeed = 2;
		double stDev = Double.parseDouble(c.readLine("Type in standard deviation: "));	//6.86;
		double mean = 0.0;
		double cutOffAff = Double.parseDouble(c.readLine("Type in cutoff affinity: ")); // 33.5; 
		double targetAff = 50.16;

		PolymerOneProtein p;
		RandomProteinDNASystem s;

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/AffTestsForAffProt/OneProt/DNA" 
								+ length + "Seeds" + numOfSeeds + "stDev" + stDev
								+ "cutOffAff" + cutOffAff + ".dat"));
		double[] targetTimes = new double[numOfSeeds];
		double DNAAff = 0.0;
		
		/*
		 * Last minute change of important settings
		 */
		if (c.readLine("Do you want to change advanced settings? y/n").equalsIgnoreCase("y")) {
			AMSeed = Long.parseLong(c.readLine("Type in affinity manager seed: "));
			DNAAff = Double.parseDouble(c.readLine("Type in initial DNA affinity: "));
			String outputName = c.readLine("Type special output name to make it clear that advanced settings" +
					"have been chosen: ");
			printer = new PrintWriter(new FileWriter("../Data/AffTestsForAffProt/OneProt/" + outputName));
		}
		
		for (; DNAAff<12; DNAAff+=1.0) {

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerOneProtein(box,length,rndm);
				s = new RandomProteinDNASystem(p, targetPos, box, DNAAff,
						mean, stDev, targetAff, cutOffAff, AMSeed);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + DNAAff + " simulated.");
			}
			printer.println(DNAAff + "\t" + averageOfArray(targetTimes));
			printer.flush();
		}

		printer.close();
	}

	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}
}