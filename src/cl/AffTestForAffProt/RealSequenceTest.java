package cl.AffTestForAffProt;

import java.io.Console;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.GenomeReader.GenomeManagerMisMatch;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class RealSequenceTest {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		Console c = System.console();
		Random rndm = new Random(3);
		int length = Integer.parseInt(c.readLine("Type in DNA length: "));
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		int numOfSeeds = Integer.parseInt(c.readLine("Type in number of seeds: "));
		double[] targetTimes = new double[numOfSeeds];
		double[] onDNATimes = new double[numOfSeeds];
		double DNAAff = Double.parseDouble(c.readLine("Type in initial DNA affinity: "));
		double finalDNAAff = Double.parseDouble(c.readLine("Type in final (not included) DNA affinity: "));

		PolymerOneProtein p;
		RandomProteinDNASystem s;

		/*
		 * Set up genome
		 */
		String filePath = "../Data/Genomes/Ecoli_K12_MG1655.fna"; //Ecoli_K12_DH10B.fna"; //chr1.fa";
		GenomeManagerMisMatch gm = new GenomeManagerMisMatch();
		gm.ReadInGenome(filePath);

		double[] affinities = gm.computeAffinities(c.readLine("Type in binding motif: "), 
				length	, targetPos, Integer.parseInt(c.readLine("Type in peak affinity")), "Highest");

		//Define output file for simulation
		String outputName = c.readLine("Type special output name to make it clear that advanced settings" +
				"have been chosen: ");
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/AffTestsForAffProt/RealProt/" + outputName));

		for (; DNAAff<finalDNAAff; DNAAff+=1.0) {

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerOneProtein(box,length,rndm);
				s = new RandomProteinDNASystem(p, targetPos, box, DNAAff, affinities);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				onDNATimes[seed] = s.getOnDNATime();
				double percentOfTimeOnDNA = (100.0*s.getOnDNATime()/((double)(s.getTime())));
				System.out.println("Search time: " + s.getTime() + "\tTime spent on DNA: " + s.getOnDNATime()
						+ " and in percent: " + percentOfTimeOnDNA);
				System.out.println("Seed " + seed + "\tAffinity " + DNAAff + " simulated.");
			}
			printer.println(DNAAff + "\t" + averageOfArray(targetTimes) + "\t" + computeError(targetTimes)
					+ "\t" + computeStandardErrorofMean(targetTimes) + "\t" + averageOfArray(onDNATimes)
					+ "\t" + computeError(onDNATimes)+ "\t" + computeStandardErrorofMean(onDNATimes));
			printer.flush();
		}

		printer.close();
	}

	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}

	private static double computeError(double[] array) {
		double mean = averageOfArray(array);
		System.out.println("mean = " + mean);
		double xMinusMeanSquared = 0;
		for (int i=0; i<array.length; i++) {
			System.out.println(array[i]);
			xMinusMeanSquared += (array[i] - mean)*(array[i] - mean);
		}
		System.out.println("final: " + xMinusMeanSquared);
		System.out.println(Math.sqrt(xMinusMeanSquared/(array.length)));
		return Math.sqrt(xMinusMeanSquared/(array.length));
	}

	private static double computeStandardErrorofMean(double[] array) {
		double stDev = computeError(array);
		return stDev/(Math.sqrt(array.length));
	}
}