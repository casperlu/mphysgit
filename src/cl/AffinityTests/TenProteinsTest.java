package cl.AffinityTests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerManyProteins;
import cl.ProteinDNASystem.ManyProteinsDNASystem;

public class TenProteinsTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Random rndm = new Random(3);
		int length = Integer.parseInt(args[0]);
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		double affinity = 1;
		int numOfSeeds = Integer.parseInt(args[1]);

		PolymerManyProteins p = new PolymerManyProteins(box,length,rndm);

		ManyProteinsDNASystem s = new ManyProteinsDNASystem(p, targetPos, box, affinity, 10);

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/ManyProtein/Prot10DNA" + length + "Seeds"
																	+ numOfSeeds + ".dat"));
		double[] targetTimes = new double[numOfSeeds];

		for (double aff=0.0; aff<13; aff+=1.0) {

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerManyProteins(box,length,rndm);
				s = new ManyProteinsDNASystem(p, targetPos, box, aff, 10);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + aff + " simulated.");
			}
			printer.println(aff + "\t" + averageOfArray(targetTimes) + "\t" + computeError(targetTimes)
						+ "\t" + computeStandardErrorofMean(targetTimes));
			printer.flush();
		}

		printer.close();
	}
	
		private static double averageOfArray(double[] array) {
			double value = 0;
			for (int i=0; i<array.length; i++) {
				value += array[i];
			}
			return value/(array.length);
		}

		private static double computeError(double[] array) {
			double mean = averageOfArray(array);
			double xMinusMeanSquared = 0;
			for (int i=0; i<array.length; i++) {
				System.out.println(array[i]);
				xMinusMeanSquared += (array[i] - mean)*(array[i] - mean);
			}
			return Math.sqrt(xMinusMeanSquared/(array.length));
		}

		private static double computeStandardErrorofMean(double[] array) {
			double stDev = computeError(array);
			return stDev/(Math.sqrt(array.length));
		}

}
