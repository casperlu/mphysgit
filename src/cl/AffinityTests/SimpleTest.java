package cl.AffinityTests;

import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerOneProtein;
import cl.ProteinDNASystem.RandomProteinDNASystem;

public class SimpleTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Console c = System.console();
		Random rndm;
		int length = Integer.parseInt(c.readLine("Type in DNA length: "));
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		int numOfSeeds = Integer.parseInt(c.readLine("Type in number of seeds: "));
		
		PolymerOneProtein p;
		RandomProteinDNASystem s;

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/NewAffTests/OneProt/DNA" 
								+ length + "Seeds" + numOfSeeds + ".dat"));
		double[] targetTimes = new double[numOfSeeds];
		double aff = 0.0;
		
		//Changed advanced settings
		if (c.readLine("Do you want to change advanced settings? y/n ").equalsIgnoreCase("y")) {
			aff = Double.parseDouble(c.readLine("Type in new start affinity: "));
		}

		//Run simulations
		for (; aff<12; aff+=0.5) {

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerOneProtein(box,length,rndm);
				s = new RandomProteinDNASystem(p, targetPos, box, aff);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + aff + " simulated.");
			}
			printer.println(aff + "\t" + averageOfArray(targetTimes) + "\t" + computeError(targetTimes)
					+ "\t" + computeStandardErrorofMean(targetTimes));
			printer.flush();
		}

		printer.close();
	}
	
	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}
	
	private static double computeError(double[] array) {
		double mean = averageOfArray(array);
		System.out.println("mean = " + mean);
		double xMinusMeanSquared = 0;
		for (int i=0; i<array.length; i++) {
			System.out.println(array[i]);
			xMinusMeanSquared += (array[i] - mean)*(array[i] - mean);
		}
		System.out.println("final: " + xMinusMeanSquared);
		System.out.println(Math.sqrt(xMinusMeanSquared/(array.length)));
		return Math.sqrt(xMinusMeanSquared/(array.length));
	}
	
	private static double computeStandardErrorofMean(double[] array) {
		double stDev = computeError(array);
		return stDev/(Math.sqrt(array.length));
	}
}
