package cl.AffinityTests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerManyProteins;
import cl.ProteinDNASystem.ManySpecificDNASystem;

public class FiveSpecificTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Random rndm = new Random(3);
		int length = Integer.parseInt(args[0]);
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		double affinity = 1;
		int distFromTarget = Integer.parseInt(args[1]);
		int numOfSeeds = Integer.parseInt(args[2]);

		PolymerManyProteins p = new PolymerManyProteins(box,length,rndm);


		ManySpecificDNASystem s = new ManySpecificDNASystem(p, targetPos, box, affinity, 5, distFromTarget);

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/ManySpecificProtein/FiveProteins/Dist"+
								distFromTarget + "DNA" + length + "Seeds" + numOfSeeds + ".dat"));
		double[] targetTimes = new double[numOfSeeds];

		for (double aff=0.0; aff<13; aff+=1.0) {

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerManyProteins(box,length,rndm);
				s = new ManySpecificDNASystem(p, targetPos, box, aff, 5, distFromTarget);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getTargetStatus() == false) {
					s.update();
				}
				targetTimes[seed] = s.getTime();
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + aff + " simulated.");
			}
			printer.println(aff + "\t" + averageOfArray(targetTimes));
			printer.flush();
		}

		printer.close();
	}
	
	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}
}
