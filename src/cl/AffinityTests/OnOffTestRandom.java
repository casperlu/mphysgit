package cl.AffinityTests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerManyProteins;
import cl.ProteinDNASystem.OnOffSystemRandom;

public class OnOffTestRandom {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		Random rndm = new Random(3);
		int length = Integer.parseInt(args[0]);
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		double polymeraseAff = Double.parseDouble(args[1]);
		int numOfSeeds = Integer.parseInt(args[2]);
		
		PolymerManyProteins p = new PolymerManyProteins(box,length,rndm);


		OnOffSystemRandom s = new OnOffSystemRandom(p, targetPos, box, polymeraseAff, 1);

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("../Data/ManySpecificProtein/OnOff/RandomDNA"
				+ length + "PolyAff" + polymeraseAff + "Seeds" + numOfSeeds + ".dat"));
		double[] targetTimes = new double[numOfSeeds];
		int onCount, offCount;

		for (double TFaff=0.0; TFaff<13; TFaff+=1.0) {
			
			onCount = 0;
			offCount = 0;

			for (int seed=0; seed<numOfSeeds; seed++) {
				rndm = new Random(seed);
				p = new PolymerManyProteins(box,length,rndm);
				s = new OnOffSystemRandom(p, targetPos, box, polymeraseAff, TFaff);
				s.initialise();		//Implicit equilibration time = 25000
				while (s.getOnOff() == -1) {
					s.update();
				}
				if (s.getOnOff() == 0) { offCount ++; }
				else if (s.getOnOff() == 1) { onCount ++; }
				else { throw new IllegalStateException("System thinks target is reached. However, " +
						"getOnOff doesn't return 0 or 1!"); }
				
				targetTimes[seed] = s.getTime();
				System.out.println(s.getTime());
				System.out.println("Seed " + seed + "\tAffinity " + TFaff + " simulated.");
			}
			printer.println(TFaff + "\t" + averageOfArray(targetTimes) + "\t" + onCount
					+ "\t" + offCount + "\t" + (onCount/((double)(onCount+offCount))));
			printer.flush();
		}

		printer.close();
	}
	
	private static double averageOfArray(double[] array) {
		double value = 0;
		for (int i=0; i<array.length; i++) {
			value += array[i];
		}
		return value/(array.length);
	}
}
