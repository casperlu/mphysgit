package cl.ProtConfigurations;

import cl.Configurations.Configuration;
import cl.Lattice.Map;

public enum JumpOffEnd implements Configuration {

	XPLUS {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(-1,0,0);
			Map m2 = new Map(0,1,0);
			Map m3 = new Map(0,-1,0);
			Map m4 = new Map(0,0,1);
			Map m5 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4,m5};
		}
	},

	XMINUS {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,1,0);
			Map m3 = new Map(0,-1,0);
			Map m4 = new Map(0,0,1);
			Map m5 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4,m5};
		}
	},

	YPLUS {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,-1,0);
			Map m4 = new Map(0,0,1);
			Map m5 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4,m5};
		}
	},

	YMINUS {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,0,1);
			Map m5 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4,m5};
		}
	},

	ZPLUS {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			Map m5 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4,m5};
		}
	},

	ZMINUS {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			Map m5 = new Map(0,0,1);
			return new Map[] {m1,m2,m3,m4,m5};
		}
	};

	/*
	 * Abstract method that when called gives all the possible
	 * moves for a certain configuration. The method must be 
	 * implemented in all enum constants.
	 */
	public abstract Map[] getPossibleMoves();


	/*
	 * Definition of relative maps for crankshaft configurations.
	 * See notebook drawings.
	 */	
	private final static Map[] endMaps = new Map[] {
		new Map(1,0,0),		//XPLUS
		new Map(-1,0,0),	//XMINUS	
		new Map(0,1,0),		//YPLUS
		new Map(0,-1,0),	//YMINUS
		new Map(0,0,1),		//ZPLUS
		new Map(0,0,-1)		//ZMINUS	
	};

	public static Map[] getEndMaps() { return endMaps; }

	/*
	 * Array of enums
	 */
	private final static JumpOffEnd[] endArray = new JumpOffEnd[] {
		XPLUS,
		XMINUS,
		YPLUS,
		YMINUS,
		ZPLUS,
		ZMINUS
	};

	public static JumpOffEnd[] getConfigurations() {
		return endArray;
	}
}
