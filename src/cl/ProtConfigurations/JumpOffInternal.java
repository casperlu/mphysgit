package cl.ProtConfigurations;

import cl.Configurations.Configuration;
import cl.Lattice.Map;

public enum JumpOffInternal implements Configuration {

	X1 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},

	X2 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X3 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X4 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(1,0,0);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X5 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(1,0,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X7 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(-1,0,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X8 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X9 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(-1,0,0);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	X10 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(-1,0,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},

	Y1 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y2 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,1,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y3 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y4 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(1,0,0);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y5 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(1,0,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y7 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,-1,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y8 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,-1,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y9 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,-1,0);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Y10 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z1 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z2 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,0,1);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z3 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,0,1);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z4 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,1);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z5 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,0,1);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z7 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(0,0,-1);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z8 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(0,0,-1);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z9 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,0,-1);
			Map m4 = new Map(0,-1,0);
			return new Map[] {m1,m2,m3,m4};
		}
	},
	
	Z10 {
		public Map[] getPossibleMoves() {
			Map m1 = new Map(1,0,0);
			Map m2 = new Map(-1,0,0);
			Map m3 = new Map(0,1,0);
			Map m4 = new Map(0,0,-1);
			return new Map[] {m1,m2,m3,m4};
		}
	};

	/*
	 * Abstract method that when called gives all the possible
	 * moves for a certain configuration. The method must be 
	 * implemented in all enum constants.
	 */
	public abstract Map[] getPossibleMoves();


	/*
	 * Definition of relative maps for crankshaft configurations.
	 * See notebook drawings.
	 */	
	private final static Map[][] internalMaps = new Map[][] {
		{new Map(1,0,0), new Map(1,0,0)},	//X1
		{new Map(1,0,0), new Map(0,1,0)},	//X2
		{new Map(1,0,0), new Map(0,-1,0)},	//X3
		{new Map(1,0,0), new Map(0,0,1)},	//X4
		{new Map(1,0,0), new Map(0,0,-1)},	//X5
		{new Map(-1,0,0), new Map(-1,0,0)},	//X6
		{new Map(-1,0,0), new Map(0,1,0)},	//X7
		{new Map(-1,0,0), new Map(0,-1,0)},	//X8
		{new Map(-1,0,0), new Map(0,0,1)},	//X9
		{new Map(-1,0,0), new Map(0,0,-1)},	//X10
		
		{new Map(0,1,0), new Map(0,1,0)},	//Y1
		{new Map(0,1,0), new Map(1,0,0)},	//Y2
		{new Map(0,1,0), new Map(-1,0,0)},	//Y3
		{new Map(0,1,0), new Map(0,0,1)},	//Y4
		{new Map(0,1,0), new Map(0,0,-1)},	//Y5
		{new Map(0,-1,0), new Map(0,-1,0)},	//Y6
		{new Map(0,-1,0), new Map(1,0,0)},	//Y7
		{new Map(0,-1,0), new Map(-1,0,0)},	//Y8
		{new Map(0,-1,0), new Map(0,0,1)},	//Y9
		{new Map(0,-1,0), new Map(0,0,-1)},	//Y10
		
		{new Map(0,0,1), new Map(0,0,1)},	//Z1
		{new Map(0,0,1), new Map(1,0,0)},	//Z2
		{new Map(0,0,1), new Map(-1,0,0)},	//Z3
		{new Map(0,0,1), new Map(0,1,0)},	//Z4
		{new Map(0,0,1), new Map(0,-1,0)},	//Z5
		{new Map(0,0,-1), new Map(0,0,-1)},	//Z6
		{new Map(0,0,-1), new Map(1,0,0)},	//Z7
		{new Map(0,0,-1), new Map(-1,0,0)},	//Z8
		{new Map(0,0,-1), new Map(0,1,0)},	//Z9
		{new Map(0,0,-1), new Map(0,-1,0)},	//Z10
		
	};

	public static Map[][] getInternalMaps() { return internalMaps; }

	/*
	 * Array of enums
	 */
	private final static JumpOffInternal[] internalArray = new JumpOffInternal[] {
		X1,X2,X3,X4,X5,X1,X7,X8,X9,X10,	//X6 == X1
		Y1,Y2,Y3,Y4,Y5,Y1,Y7,Y8,Y9,Y10,	//Y6 == Y1
		Z1,Z2,Z3,Z4,Z5,Z1,Z7,Z8,Z9,Z10	//Z6 == Z1
	};

	public static JumpOffInternal[] getConfigurations() {
		return internalArray;
	}
}
