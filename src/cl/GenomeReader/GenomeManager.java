package cl.GenomeReader;

import java.io.FileNotFoundException;

public class GenomeManager {

	//Internal variables
	private Reader r;
	private Searcher s;
	private char[][] genome;

	/*
	 * Constructor
	 */
	public GenomeManager() {	}

	/*
	 * Method for loading in the genome from a filepath to a data file containing the genome in fasta format (.fna)
	 */
	public void ReadInGenome(String filePath) throws FileNotFoundException {
		r = new Reader(filePath);
		genome = r.getGenome();
		s = new Searcher(genome);
	}

	/*
	 * Method for looking up a specific nucleotide sequence. Returns position of first nucleotide.
	 */
	public int[] searchForString(String searchText) {
		return s.search(searchText);
	}

	/*
	 * Method for looking up a sequence of a certain length at a certain position
	 */
	public char[] getSequenceAtBp(int bp, int sequenceLength) { return s.lookUpBp(bp, sequenceLength); }

	/*
	 * Method for getting  a sequence of a certain length with a user-specified binding motif at
	 * a specific position in the sequence.
	 * Inputs in monomers (e.g. length = 1000, targetPos = 500)
	 */
	public char[] getSequence(String searchText, int length, int targetPos) {

		//Define binding motif as char[]. This also defines the size of a monomer
		char[] s = searchText.toCharArray();
		int sizeOfMonomer = s.length;

		//Search for binding motif
		int[] bindingSites = searchForString(searchText);
		int numOfSites = bindingSites.length;
		int genomeLength = r.getGenomeLength();

		//char[] to store sequence we wish to return to user
		int returnSequenceLength = sizeOfMonomer*(length+1); //Must be 1 larger as it needs to store extra data to the right
		char[] returnSequence = new char[returnSequenceLength];

		//Positions in bp units (relative to binding site)
		int relLeftMostPos =  -targetPos*sizeOfMonomer;
		int relRightMostPos = (length - targetPos+1)*sizeOfMonomer; //Again 1 extra monomer to the right

		//If a binding site is found, continue
		if (numOfSites>1) {

			//Run through all sites until a suitable one is found
			for (int i=0; i<numOfSites; i++) {

				//Actual positions for binding site i
				int leftMostPos = (bindingSites[i] + relLeftMostPos + genomeLength)%genomeLength;
				int rightMostPos = (bindingSites[i] + relRightMostPos)%genomeLength;

				/*
				 * If binding site before current binding site is outside the interval we are interested in
				 * && binding site after current binding site is outside the interval we are intersted in
				 * then we have a good sequence
				 */
				if (inInterval(leftMostPos, rightMostPos, bindingSites[(i-1+numOfSites)%numOfSites]) == false
						&& inInterval(leftMostPos, rightMostPos, bindingSites[(i+1)%numOfSites]) == false) {

					//Store correct sequence
					for (int j=0; j<returnSequenceLength; j++) {
						returnSequence[j] = r.lookUpBp((j+leftMostPos)%genomeLength);
					}
					return returnSequence;
				}
			}
		}

		//If there is only one binding site we are ensured to have a good sequence
		else if(numOfSites == 1) {

			//Actual positions for binding site i
			int leftMostPos = (bindingSites[0] + relLeftMostPos + genomeLength)%genomeLength;

			//Store correct sequence
			for (int j=0; j<returnSequenceLength; j++) {
				returnSequence[j] = r.lookUpBp((j+leftMostPos)%genomeLength);
			}

			return returnSequence;
		}


		//Else throw some exception here
		System.out.println("ERROR: No sequence found with that binding motif!");
		return null;

	}

	/*
	 * Method for checking if a number is in an interval (when interval can wrap around 0)
	 */
	private boolean inInterval(int left, int right, int number) {

		//Straight forward case of left < right
		if (left < right) {
			if (number<left || number>right) {
				return false;	//Not in interval
			}
		}

		//Confusing case where left > right due to periodic boundary conditions
		else if (left > right) {
			if (number > right && number < left) {
				return false;	//Not in interval
			}
		}

		return true;	//In interval
	}
}