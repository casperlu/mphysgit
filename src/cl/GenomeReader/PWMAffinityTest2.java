package cl.GenomeReader;

import java.io.Console;
import java.io.FileWriter;
import java.io.PrintWriter;

import cl.PWM.PWM;
import cl.PWM.PWMProducer;

public class PWMAffinityTest2 {

	/**
	 * @param args
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws NumberFormatException, Exception {

		//Set up motifs and feed them to producer
		String[] bindingMotifsOfLacI = new String[] {"AAATGTNNNNNNNNNACAACC", "AATTGTNNNNNNNNNACAATT", "AATTGCNNNNNNNNNACTGCC"}; //"GGCAGTNNNNNNNNNGCAATT"}; // "AATTGCNNNNNNNNNACTGCC"};
		PWMProducer producer = new PWMProducer(bindingMotifsOfLacI);

		//Compute PWM. Takes epsilonStar as argument
		PWM pwm = producer.computePWM(1);

		Console c = System.console();
		String filePath = "../Data/Genomes/Ecoli_K12_MG1655.fna"; //Ecoli_K12_DH10B.fna"; //chr1.fa";
		GenomeManagerPWM gm = new GenomeManagerPWM(pwm);
		gm.ReadInGenome(filePath);

		double[] allAffinities = gm.computeWholeGenome(c.readLine("Type in search text: "), Integer.parseInt(c.readLine("Type in DNA length: "))
				, Integer.parseInt(c.readLine("Type in targetPosition: ")));
		double binSize = 1;
		double[][] histOfHigh = generateHistogram(allAffinities, binSize);
		
		//Introduce LinearTransformer with data analysis properties
		LinearTransformer lt = new LinearTransformer();
		double mean = lt.getMean(allAffinities);
		double stDev = lt.getStDev(allAffinities, mean);
		System.out.println("Mean: " + mean + " stDev: " + stDev);

		PrintWriter output3 = new PrintWriter(new FileWriter("allPWMAff.out"));
		PrintWriter output4 = new PrintWriter(new FileWriter("histAllPWMAff.out"));
		PrintWriter output5 = new PrintWriter(new FileWriter("histAllFit.plot"));

		for (int i=0; i<allAffinities.length; i++) {
			output3.println(i + "\t" + allAffinities[i]);
		}
		for (int i=0; i<histOfHigh.length; i++) {
			output4.println(histOfHigh[i][0] + "\t" + histOfHigh[i][1]);
		}
		output3.close();
		output4.close();
		
		//Write histogram plot file for gnuplot
		output5.println("mean = " + mean);
		output5.println("stDev = "+ stDev);
		output5.println("gaussian(x) = A*exp(-(x-mean)*(x-mean)/(2*stDev*stDev)) + B");
		output5.println("fit gaussian(x) \"./histAllPWMAff.out\" u 1:2 via A,B");
		output5.println("plot \"./histAllPWMAff.out\" u 1:2 with boxes");
		output5.println("replot gaussian(x)");
		output5.close();
	}

	public static double[][] generateHistogram(double[] data, double binSize) {
		//Find max and min
		double max = -Double.MAX_VALUE;
		double min = Double.MAX_VALUE;
		for (int i=0; i<data.length; i++) {
			if (data[i] > max) { max = data[i]; }
			else if (data[i] < min) { min = data[i]; }
		}

		double interval = max - min;
		int numOfBins = ((int) (interval/binSize)) +1;
		double[][] hist = new double[numOfBins][2];

		//Reset
		for (int i=0; i<numOfBins; i++) { hist[i][1] = 0; }


		for (int i=0; i< data.length; i++) {
			double currentMin = Math.floor(min);
			double currentMax = currentMin+binSize;

			for (int j=0; j<numOfBins; j++) {
				hist[j][0] = currentMin + binSize/2;
				if (data[i] >= currentMin && data[i] < currentMax) {
					hist[j][1]++;
				}
				currentMin += binSize;
				currentMax += binSize;
			}
		}
		return hist;
	}
}