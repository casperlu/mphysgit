package cl.GenomeReader;

import java.io.Console;
import java.io.FileWriter;
import java.io.PrintWriter;

public class SearchTest {

	/**
	 * @param args
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws NumberFormatException, Exception {

		Console c = System.console();
		String filePath = "../Data/Genomes/Ecoli_K12_MG1655.fna"; //Ecoli_K12_DH10B.fna"; //chr1.fa";
		GenomeManagerMisMatch gm = new GenomeManagerMisMatch();
		gm.ReadInGenome(filePath);

		/*
		 * Look up basepair and print to screen
		 */
		char [] lookUp = gm.getSequenceAtBp(Integer.parseInt(c.readLine("Look up bp: ")),
				Integer.parseInt(c.readLine("Type in desired length of sequence: ")));
		for (int i=0; i<lookUp.length; i++) { System.out.print(lookUp[i]); }





		int[] matches = gm.searchForString(c.readLine("Type in search text: "));

		for (int i=0; i<matches.length; i++) { System.out.println(matches[i]); }

		char[] goodSequence = gm.getSequence(c.readLine("Type in search text: "), 10, 5);
		System.out.print(goodSequence);

		double[] blockAffinities = gm.computeBlockAffinities(c.readLine("Type in search text: "), Integer.parseInt(c.readLine("Type in DNA length: "))
				, Integer.parseInt(c.readLine("Type in targetPosition: ")), Integer.parseInt(c.readLine("Type in peak affinity: ")));
		double[] averageAffinities = gm.computeAffinities(c.readLine("Type in search text: "), Integer.parseInt(c.readLine("Type in DNA length: "))
				, Integer.parseInt(c.readLine("Type in targetPosition: ")),
				Integer.parseInt(c.readLine("Type in peak affinity: ")), "Average");
		double[] highestAffinities = gm.computeAffinities(c.readLine("Type in search text: "), Integer.parseInt(c.readLine("Type in DNA length: "))
				, Integer.parseInt(c.readLine("Type in targetPosition: ")),
				Integer.parseInt(c.readLine("Type in peak affinity: ")),"Highest");
		double[][] histOfHigh = generateHistogram(highestAffinities, 0.2);

		PrintWriter output1 = new PrintWriter(new FileWriter("blockAff.out"));
		PrintWriter output2 = new PrintWriter(new FileWriter("averageAff.out"));
		PrintWriter output3 = new PrintWriter(new FileWriter("highestAff.out"));
		PrintWriter output4 = new PrintWriter(new FileWriter("histHighest.out"));

		for (int i=0; i<blockAffinities.length; i++) {
			output1.println(i + "\t" + blockAffinities[i]);
			output2.println(i + "\t" + averageAffinities[i]);
			output3.println(i + "\t" + highestAffinities[i]);
		}
		for (int i=0; i<histOfHigh.length; i++) {
			output4.println(histOfHigh[i][0] + "\t" + histOfHigh[i][1]);
		}
		output1.close();
		output2.close();
		output3.close();
		output4.close();
	}

	public static double[][] generateHistogram(double[] data, double binSize) {
		//Find max and min
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (int i=0; i<data.length; i++) {
			if (data[i] > max) { max = data[i]; }
			else if (data[i] < min) { min = data[i]; }
		}

		double interval = max - min;
		int numOfBins = (int) (interval/binSize) +1;
		double[][] hist = new double[numOfBins][2];
		
		//Reset
		for (int i=0; i<numOfBins; i++) { hist[i][1] = 0; }

	
		for (int i=0; i< data.length; i++) {
			double currentMin = min;
			double currentMax = min+binSize;
			
			for (int j=0; j<numOfBins; j++) {
				hist[j][0] = currentMin + binSize/2;
				if (data[i] >= currentMin && data[i] < currentMax) {
					hist[j][1]++;
				}
				currentMin += binSize;
				currentMax += binSize;
			}
		}
		return hist;
	}
}
