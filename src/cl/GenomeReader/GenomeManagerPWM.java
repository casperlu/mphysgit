package cl.GenomeReader;

import cl.PWM.PWM;

public class GenomeManagerPWM extends GenomeManager {

	//Internal variables
	private LinearTransformer trans = new LinearTransformer();
	private PWM pwm;

	/*
	 * Constructor takes already constructed Position Weight Matrix
	 */
	public GenomeManagerPWM(PWM positionWeightMatrix) {
		pwm = positionWeightMatrix;
	}

	/*
	 * Computing average affinities in monomers using PWM algorithm.
	 * Affinities are computing depending on the input String "method".
	 * Choose method = "Average" or method = "Highest".
	 */
	public double[] computeAffinities(String bindingMotif, int length, int targetPos, String method) throws Exception {

		//Define binding motif as char[]. This also defines the size of a monomer
		char[] s = bindingMotif.toCharArray();
		int sizeOfMonomer = s.length;

		//Get sequence
		char[] sequence = getSequence(bindingMotif, length, targetPos);	//Sequence is of length L+1 (e.g. 1000 --> 1001)
		double[] affinities = new double[length];	//Placeholder for affinities

		//Loop through all monomers
		for (int i=0; i<length; i++) {
			double accumulatedAffinity = 0;
			double highestAff = -Double.MAX_VALUE;

			//Loop through all bps in monomer
			for (int j=0; j<sizeOfMonomer; j++) {

				//Reset energy to zero
				double energy = 0;
				
				//Get sequence for current bp
				for (int k=0; k<sizeOfMonomer; k++) {

					//Get energies from PWM
					energy += pwm.getPWM(k, sequence[i*sizeOfMonomer+j+k]); //Remember sequence is longer than L, so it works
				}

				//Average affinity method
				if (method.equalsIgnoreCase("Average")) {
					accumulatedAffinity += energy;
				}

				//Highest affinity method
				else if (method.equalsIgnoreCase("Highest")); {
					if (energy > highestAff) { highestAff = energy; }
				}
			}

			//Average affinity method
			if (method.equalsIgnoreCase("Average")) {
				affinities[i] = accumulatedAffinity/sizeOfMonomer;
			}

			//Highest affinity method
			else if (method.equalsIgnoreCase("Highest")) {
				affinities[i] = highestAff;
			}
		}

		//If correct method, then scale affinities and return them
		if (method.equalsIgnoreCase("Average") || method.equalsIgnoreCase("Highest")) {

			/*
			 * Scaling currently not taking place
			 */
			//affinities = scaleToAverageEcoli(affinities);
			return affinities;
		}

		//Otherwise user screwed up and exception should be thrown
		else { throw new Exception("Method must be \"Average\" or \"Highest\""); }
	}


	/*
	 * Compute affinities in blocks of the length of the monomer
	 */
	public double[] computeBlockAffinities(String bindingMotif, int length, int targetPos, double realPeakAff) {

		//Define binding motif as char[]. This also defines the size of a monomer
		char[] s = bindingMotif.toCharArray();
		int sizeOfMonomer = s.length;

		//Get sequence
		char[] sequence = getSequence(bindingMotif, length, targetPos);	//Sequence is of length L+1 (e.g. 1000 --> 1001)
		double[] affinities = new double[length];	//Placeholder for affinities

		//Loop through all monomers
		for (int i=0; i<length; i++) {

			//Reset energy
			double energy = 0;

			//Loop through all bps in monomer
			for (int j=0; j<sizeOfMonomer; j++) {
				//Get energies from PWM
				energy += pwm.getPWM(j, sequence[i*sizeOfMonomer+j]); //Remember sequence is longer than L, so there is a bit of waste here, but this is not imporant
			}
			affinities[i] = energy;
		}	

		//Scaling currently not used
		//affinities = scaleToAverageEcoli(affinities);
		return affinities;
	}

	/*
	 * Computing affinites for all nucleotides in E. coli!
	 */
	public double[] computeWholeGenome(String bindingMotif, int length, int targetPos) throws Exception {

		//Define binding motif as char[]. This also defines the size of a monomer
		char[] s = bindingMotif.toCharArray();
		int sizeOfMonomer = s.length;

		//Get sequence
		char[] sequence = getSequence(bindingMotif, length, targetPos);	//Sequence is of length L+1 (e.g. 1000 --> 1001)
		double[] affinities = new double[length*sizeOfMonomer];	//Placeholder for affinities
		int counter = 0;

		//Loop through all monomers
		for (int i=0; i<length; i++) {

			//Loop through all bps in monomer
			for (int j=0; j<sizeOfMonomer; j++) {

				//Reset energy to zero
				double energy = 0;

				//Get sequence for current bp
				for (int k=0; k<sizeOfMonomer; k++) {

					//Get energies from PWM
					energy += pwm.getPWM(k, sequence[i*sizeOfMonomer+j+k]); //Remember sequence is longer than L, so it works
				}

				//Save value in array
				affinities[counter++] = energy;
			}
		}
		return affinities;
	}


	/*
	 * Method for scaling a set of data to the average values of E. coli
	 */
	public double[] scaleToAverageEcoli(double[] affs) {
		double eColiAverage = 2.47;
		double eColiStDev = 2.16;
		return trans.transform(affs, eColiAverage, eColiStDev);
	}
}
