package cl.GenomeReader;

public class Searcher {

	//Internal variables
	private char[][] g;

	public Searcher(char[][] genome) {
		g = genome;
	}

	public int[] search(String searchText) {
		int[] largeArray = new int[99999];	//There might be a better way than this
		char[] s = searchText.toCharArray();
		int length = s.length;
		char[] temp = new char[length];
		int arrayCounter = 0;
		int bpCounter = 0;

		for (int i=0; i<g.length; i++) {
			for (int j=0; j<g[i].length; j++) {
				//Read in sequence at j to temp
				for (int k=0; k<length; k++) {
					temp[k] = g[(i + (j+k)/g[i].length)%g.length][(j+k)%g[i].length];
				}
				if (compareCharArrays(temp,s) == true) {
					largeArray[arrayCounter] = bpCounter; //i*g[i].length + j;
					arrayCounter++;
				}
				bpCounter ++;
			}
		}

		int[] result = new int[arrayCounter];
		for (int i=0; i<arrayCounter; i++) {
			result[i] = largeArray[i];
		}
		return result;
	}

	/*
	 * Look up the sequence following a specific basepair number
	 */
	public char[] lookUpBp(int bp, int lengthOfSequence) {
		char[] result = new char[lengthOfSequence];

		int counter = 0;
		int line = 0;
		int pos = 0;
		for (int i=0; i<g.length; i++) {
			line = i;
			for (int j=0; j<g[i].length; j++) {
				counter++;
				pos = j;
				if (counter == bp) { break; }
			}
			if (counter == bp) { break; }
		}

		for (int i=0; i<lengthOfSequence; i++) {
			//Assumes lines are of same length (only marginally dodgy)
			result[i] = g[(line + (pos+i)/g[line].length)%g.length][(pos+i)%g[line].length];
		}

		return result;
	}

	static boolean compareCharArrays(char[] a, char[] b) {
		for (int i=0; i<a.length; i++) {
			if (a[i] != b[i]) { return false; }
		}
		return true;
	}
}
