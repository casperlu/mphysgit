package cl.GenomeReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Reader {

	private String genomePath;
	private int fullGenomeLength;
	private char[][] genome;
	private int numOfLines;

	public Reader(String filePath) {
		genomePath = filePath;
	}

	public char[][] getGenome() throws FileNotFoundException {
		//String fileName = "../Data/Genomes/Ecoli_K12_DH10B.fna"; //c.readLine("Type in filepath for genome: ");
		BufferedReader input = new BufferedReader(new FileReader(genomePath));
		Scanner scan = new Scanner(input);

		//Assume genome on .fna form and count number of lines in data set
		numOfLines = 0;
		while (scan.hasNext()) { 
			numOfLines ++;
			scan.next();
		}

		//Set up scanner again
		input = new BufferedReader(new FileReader(genomePath));
		scan = new Scanner(input);

		//Get input and store in char[][] array
		String line;
		fullGenomeLength = 0;
		genome = new char[numOfLines][];
		for (int i=0; i<numOfLines; i++) {
			line = scan.next();
			genome[i] = line.toCharArray();	//Notice genome[i] may be of varying lengths!
			fullGenomeLength += genome[i].length;
		}
		return genome;
	}

	//Get full length of genome
	public int getGenomeLength() { return fullGenomeLength; }

	//Look up a specific basepair
	public char lookUpBp(int bp) {

		//Find line and position for said bp
		int bpCounter = 0;
		for (int line=0; line<numOfLines; line++) {
			bpCounter += genome[line].length;
			if (bpCounter > bp) { 

				//Undo last addition
				bpCounter -= genome[line].length;
				int position = bp - bpCounter;
				return genome[line][position];
			}	
		}
		return Character.MIN_VALUE;
	}
}
