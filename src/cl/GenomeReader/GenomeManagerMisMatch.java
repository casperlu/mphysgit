package cl.GenomeReader;


public class GenomeManagerMisMatch extends GenomeManager {

	//Internal variables
	private LinearTransformer trans = new LinearTransformer();

	public GenomeManagerMisMatch() {
		
	}

	/*
	 * Computing average affinities in monomers using mismatch algorithm.
	 * Affinities are computing depending on the input String "method".
	 * Choose method = "Average" or method = "Highest".
	 */
	public double[] computeAffinities(String bindingMotif, int length, int targetPos, double realPeakAff, String method) throws Exception {

		//Define binding motif as char[]. This also defines the size of a monomer
		char[] s = bindingMotif.toCharArray();
		int sizeOfMonomer = s.length;

		//Get sequence
		char[] sequence = getSequence(bindingMotif, length, targetPos);	//Sequence is of length L+1 (e.g. 1000 --> 1001)
		double[] affinities = new double[length];	//Placeholder for affinities

		//Loop through all monomers
		for (int i=0; i<length; i++) {
			double accumulatedAffinity = 0;
			double highestAff = 0;
			char[] current = new char[sizeOfMonomer];

			//Loop through all bps in monomer
			for (int j=0; j<sizeOfMonomer; j++) {

				//Get sequence for current bp
				for (int k=0; k<sizeOfMonomer; k++) {
					current[k] = sequence[i*sizeOfMonomer+j+k];	//Remember sequence is longer than L, so it works
				}

				//Get mismatch energy for sequence
				double energy = getMisMatch(s, current);

				//Average affinity method
				if (method.equalsIgnoreCase("Average")) {
					accumulatedAffinity += energy;
				}

				//Highest affinity method
				else if (method.equalsIgnoreCase("Highest")); {
					if (energy > highestAff) { highestAff = energy; }
				}
			}

			//Average affinity method
			if (method.equalsIgnoreCase("Average")) {
				affinities[i] = accumulatedAffinity/sizeOfMonomer;
			}

			//Highest affinity method
			else if (method.equalsIgnoreCase("Highest")) {
				affinities[i] = highestAff;
			}
		}

		//Scale affinities to E. coli averages
		if (method.equalsIgnoreCase("Average") || method.equalsIgnoreCase("Highest")) {
			scaleToPeakAff(affinities, realPeakAff); //Scale affinities so they match with real data
			//affinities = scaleToAverageEcoli(affinities);
			return affinities;
		}
		else { throw new Exception("Method must be \"Average\" or \"Highest\""); }
	}


	/*
	 * Compute affinities in blocks of the length of the monomer
	 * using mismatch algorithm.
	 */
	public double[] computeBlockAffinities(String bindingMotif, int length, int targetPos, double realPeakAff) {

		//Define binding motif as char[]. This also defines the size of a monomer
		char[] s = bindingMotif.toCharArray();
		int sizeOfMonomer = s.length;

		//Get sequence
		char[] sequence = getSequence(bindingMotif, length, targetPos);	//Sequence is of length L+1 (e.g. 1000 --> 1001)
		double[] affinities = new double[length];	//Placeholder for affinities

		//Loop through all monomers
		for (int i=0; i<length; i++) {
			char[] current = new char[sizeOfMonomer];

			//Loop through all bps in monomer
			for (int j=0; j<sizeOfMonomer; j++) {
				current[j] = sequence[i*sizeOfMonomer+j];	//Remember sequence is longer than L, so there is a bit of waste here, but this is not imporant
			}
			affinities[i] = getMisMatch(s, current);
		}	
		//scaleToPeakAff(affinities, realPeakAff); //Scale affinities so they match with real data
		affinities = scaleToAverageEcoli(affinities);
		return affinities;
	}


	/*
	 * Compare to char[] and get mismatch affinity energy
	 */
	private double getMisMatch(char[] motif, char[] sequence) {
		double matches = 0;

		//Run through sequence and see if it matches with motif
		for (int i=0; i<motif.length; i++) {
			if (motif[i] == sequence[i]) { matches++; }
		}
		return 2*matches;	//2kT per match
	}

	public void scaleToPeakAff(double[] affs, double realPeakAff) {
		//Find peak aff
		double peakAff = 0;
		for (int i=0; i<affs.length; i++) {
			if (affs[i] > peakAff) { peakAff = affs[i]; }
		}
		double scaleFactor = realPeakAff/peakAff;

		//Rescale all affinties
		for (int i=0; i<affs.length; i++) {
			affs[i] *= scaleFactor;
		}
	}

	public double[] scaleToAverageEcoli(double[] affs) {
		double eColiAverage = 2.47;
		double eColiStDev = 2.16;
		return trans.transform(affs, eColiAverage, eColiStDev);
	}
}