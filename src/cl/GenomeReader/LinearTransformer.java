package cl.GenomeReader;

public class LinearTransformer {

	public LinearTransformer() {}
	
	public double[] transform(double[] data, double newMean, double newStDev) {
		
		//Get mean and stDev of data
		double mean = getMean(data);
		double stDev = getStDev(data, mean);
		
		//Using: http://stats.stackexchange.com/questions/11093/rescaling-for-desired-standard-deviation
		/*
		 * Have X, need Y.
		 * Y = a + b*X --> want a,b.
		 * 
		 * Related via:
		 * meanY = a + b*meanX
		 * stDevY = |b|*stDevX
		 * 
		 * --> |b| = stDevY/stDevX
		 * --> a = meanY - b*meanX = meanY - stDevY*MeanX/stDevX
		 */
		double b = newStDev/stDev;
		double a = newMean - b*mean;
		
		//Rescale all data
		for (int i=0; i<data.length; i++) {
			data[i] = a + b*data[i];
		}
		return data;
	}
	
	public double getMean(double data[]) {
		double mean = 0;
		for (int i=0; i<data.length; i++) {
			mean += data[i];
		}
		return mean/data.length;
	}
	
	public double getStDev(double[] data, double mean) {
		double stDev = 0;
		for (int i=0; i<data.length; i++) {
			stDev += (data[i] - mean)*(data[i] - mean);
		}
		return Math.sqrt(stDev/data.length);
	}
}
