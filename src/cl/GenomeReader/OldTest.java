package cl.GenomeReader;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class OldTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		//Set up inputs
		Console c = System.console();
		String fileName = "../Data/Genomes/Ecoli_K12_DH10B.fna"; //c.readLine("Type in filepath for genome: ");
		String searchString = c.readLine("Input search text: ");
		BufferedReader input = new BufferedReader(new FileReader(fileName));
		Scanner scan = new Scanner(input);
		
		//Assume genome on .fna form
		int numOfLines = 0;
		while (scan.hasNext()) { 
			numOfLines ++;
			scan.next();
		}

		//Set up scanner again
		input = new BufferedReader(new FileReader(fileName));
		scan = new Scanner(input);

		//Get the input
		String line;
		char[][] genome = new char[numOfLines][];
		for (int i=0; i<numOfLines; i++) {
			line = scan.next();
			genome[i] = line.toCharArray();
			//System.out.println(i);
		}

		search(genome, searchString);
	}

	static int[] search(char[][] g, String searchText) {
		int[] largeArray = new int[99999];
		char[] s = searchText.toCharArray();
		int length = s.length;
		char[] temp = new char[length];
		int counter = 0;

		for (int i=0; i<g.length; i++) {
			for (int j=0; j<g[i].length; j++) {
				for (int k=0; k<length; k++) {
					temp[k] = g[(i + (j+k)/g[i].length)%g.length][(j+k)%g[i].length];
				}
				if (compareCharArrays(temp,s) == true) {
					largeArray[counter] = i*g[j].length + j;
					System.out.println("Match found on line: " + i + " position: " + j); 
					counter++;
					}
			}
		}
		
		System.out.println("Number of matches found: " + counter);
		int[] result = new int[counter];
		for (int i=0; i<counter; i++) {
			result[i] = largeArray[i];
		}
		return result;
	}

	static boolean compareCharArrays(char[] a, char[] b) {
		for (int i=0; i<a.length; i++) {
			if (a[i] != b[i]) { return false; }
		}
		return true;
	}
}
