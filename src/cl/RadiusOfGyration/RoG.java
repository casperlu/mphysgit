package cl.RadiusOfGyration;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.Monomer;
import cl.DNA.Polymer;

/*
 * Test investigating the diffusion of the
 * center of mass.
 */
public class RoG {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		//System parameters
		int[] DNAlengths = new int[] {100,200,300,500,1000};
		int[] boxes = new int[DNAlengths.length];
		//int[] totalTimes = new int[DNAlengths.length];
		for (int i=0; i<DNAlengths.length; i++) {
			boxes[i] = (int) Math.pow(DNAlengths[i]*100, 1.0/3.0);
		//	totalTimes = DNAlengths[i]*DNAlengths[i];
		}
		int numberOfSeeds = 100;
		Random rndm;
		
		int totalTime = 50000;
		int computationFrequency = 1000;
		int numOfMeasurements = totalTime/computationFrequency;
		
		//Output file in the form log(D) \t log(N)
		PrintWriter printer = null;
		
		//Loop over number of particles in the simulations
		for (int i=0; i<DNAlengths.length; i++) {
			printer = new PrintWriter(new FileWriter("RoG/RoG" + DNAlengths[i] + "Seed" + numberOfSeeds + ".dat"));

			double[][] RoGs = new double[numberOfSeeds][numOfMeasurements];

			//Loop over different seeds
			for (int seed=0; seed<numberOfSeeds; seed++) {
				rndm = new Random(seed);
				Polymer poly = new Polymer(boxes[i], DNAlengths[i], rndm);
				poly.initialise(-1,-1,-1);
				
				RoGs[seed][0] = computeRoG(poly); 	//Compute radius of gyration for initial poly
				for (int j=1; j<numOfMeasurements; j++) {
					poly.update(computationFrequency);
					RoGs[seed][j] = computeRoG(poly);
				}
				
				System.out.println("DNA length: " + DNAlengths[i] + ". Seed: " + seed + " printed.");
			}
			
			double[] RoGaveraged = computeAverageOfSeed(RoGs);
			
			for (int k=0; k<RoGaveraged.length; k++) {
				printer.println(k*computationFrequency + "\t" + RoGaveraged[k]);
			}
			printer.close();
		}

	}


	private static double[] computeAverageOfSeed(double[][] RoGs) {
		int numOfMeasurements = RoGs[0].length;
		int numOfSeeds = RoGs.length;
		double[] avg = new double[numOfMeasurements];
		
		for (int i=0; i<numOfMeasurements; i++) {
			avg[i] = 0.0;
			for (int j=0; j<numOfSeeds; j++) {
				avg[i] += RoGs[j][i];
			}
			avg[i] /= ((double) numOfSeeds);
		}
		
		return avg;
	}


	private static double computeRoG(Polymer poly) {
		int N = poly.getPoly().length;
		int[][] flags = getFlags(poly);
		double[][] r = getR(poly, flags);
		double[] rmean = getRmean(r);
		
		double RoG = 0.0;
		for (int i=0; i<N; i++) {
			for (int j=0; j<3; j++) {
				RoG += (r[i][j]-rmean[j])*(r[i][j]-rmean[j]);
			}
		}
		
		return RoG/((double) N);
	}


	private static double[][] getR(Polymer poly, int[][] flags) {
		Monomer[] p = poly.getPoly();
		double[][] r = new double[p.length][3];
		
		for (int i=0; i<p.length; i++) {
			for (int j=0; j<3; j++) {
				r[i][j] += p[i].getPos().getCoord(j) + flags[i][j]*poly.getBox();
			}
		}
		
		return r;
	}


	private static double[] getRmean(double[][] r) {
		int N = r.length;
		double[] rmean = new double[] {0.0,0.0,0.0};
		
		for (int i=0; i<N; i++) {
			for (int j=0; j<3; j++) {
				rmean[j] += r[i][j];
			}
		}
		
		rmean[0] /= ((double) N);
		rmean[1] /= ((double) N);
		rmean[2] /= ((double) N); 
		
		return rmean;
	}


	private static int[][] getFlags(Polymer poly) {
		Monomer[] p = poly.getPoly();
		int[][] flags = new int[p.length][3];
		
		for (int i=0; i<p.length; i++) {
			
			if (i==0) { flags[i] = new int[] {0,0,0}; }
			else {
				
				for (int j=0; j<3; j++) {
					if ((p[i].getPos().getCoord(j) - p[i-1].getPos().getCoord(j)) > 1) {
						flags[i][j] = flags[i-1][j] - 1;
					}
					else if ((p[i].getPos().getCoord(j) - p[i-1].getPos().getCoord(j)) < -1) {
						flags[i][j] = flags[i-1][j] + 1;
					}
					else {
						flags[i][j] = flags[i-1][j];
					}
				}
			}
		}
		
		return flags;
	}
}
