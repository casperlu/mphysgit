package cl.ManySpecificTests;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import cl.DNA.PolymerManyProteins;
import cl.ProteinDNASystem.ManySpecificDNASystem;

public class VMDTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Random rndm = new Random(3);
		int length = 500;
		int box = (int) Math.round(Math.pow((length*100), 1.0/3.0));
		int targetPos = length/2;
		double affinity = 5;

		PolymerManyProteins p = new PolymerManyProteins(box,length,rndm);

		ManySpecificDNASystem s = new ManySpecificDNASystem(p, targetPos, box, affinity, 5, 200);
		s.initialise();

		//Define output file for simulation
		PrintWriter printer = new PrintWriter(new FileWriter("Data/ManySpecificProtein/VMDTest.xyz"));

		int i = 0;
		while (s.getTargetStatus() == false) {
			s.update();
			if (i%5 == 0) {
				printer.println("" + (length+5));
				printer.println("Point = " + i);
				printer.print(s.toVMD());
			}
			i++;
		}
		printer.close();
	}

}
