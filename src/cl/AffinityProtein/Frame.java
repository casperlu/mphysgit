package cl.AffinityProtein;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

import cl.ProteinDNASystem.SimpleSystem;

/*
 * Frame associated with AffProtein
 */
public class Frame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	//Internal variables
	private Panel p;
	private SimpleSystem s;
	
	//Constructor taking a PDE object
	public Frame(SimpleSystem s) {
		
		this.s = s;
		JFrame.setDefaultLookAndFeelDecorated(true);
		setTitle("Protein position visualiser");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setPreferredSize(new Dimension(1250,550));
		
		//Add in panel with paint component
		p = new Panel(s);
		getContentPane().add(p);

		
		// Lay out the frame's contents, and put it on screen
		pack();
		setVisible(true);
	}
	
	//Repaint method
	public void repaint() {
		p.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Some action event
	}
}