package cl.AffinityProtein;

import java.util.Random;

import cl.Configurations.EndConfiguration;
import cl.DNA.PolymerOneProtein;
import cl.Lattice.Cell;
import cl.Lattice.Map;
import cl.Protein.Protein;


public class AffProtein extends Protein {
	
	//Internal variables
	protected AffinityManager AM;

	//Produce protein at random position
	public AffProtein(int boxSize, PolymerOneProtein p, Random r, 
			double target, AffinityManager affMan) {
		//Give Protein class NaN for the affinity (cheap way of not having to copy loads of code)
		super(boxSize, p, r, Double.NaN, target);
		AM = affMan;
	}
	
	//Produce protein at specific position
	public AffProtein(int boxSize, Cell position, PolymerOneProtein p,
			Random r, double target, AffinityManager affMan) {
		//Give Protein class NaN for the affinity (cheap way of not having to copy loads of code)
		super(boxSize, position, p, r, Double.NaN, target);
		AM = affMan;
	}
	
	@Override
	public void update() {

		//3D diffusion
		if (grid.getCell(pos) == -1) {
			update3dDiffusion();
		}

		//Possible 1D diffusion
		else {

			//The protein is now on the DNA
			onDNA = 1;

			//Get the index of the monomer it is attached to
			int index = grid.getCell(pos);

			//Check if monomer is the target
			if (poly.getTarget(index) == target) { targetFound = true; }


			//Only if protein is not at target, should it continue searching
			if (targetFound == false) {

				//Notify polymer that protein is no longer on monomer[index]
				poly.updateOccupation(index, -1);
				
				//If protein is attached to an end monomer
				if (index == 0 || index == (N-1)) {
					updateEndProtein(index);
				}

				//If protein is attached to an internal monomer
				else {	updateInternalProtein(index); }
			}
		}
	}

	private void update3dDiffusion() {
		//Do 3d random walk
		onDNA = 0;
		conf = EndConfiguration.ENDMONOMER;		//Protein is same as for endmonomer
		Map[] relativeMoves = conf.getPossibleMoves();

		/*
		 * Probability of either move in configuration is
		 * 1/6. Choose one at random. Add the relative move to the
		 * position of nearest to get the new move of the end
		 * monomer. Try the move: If cell is empty, perform move.
		 */
		int random = (int) (rndm.nextDouble()*6);
		Cell newMove = Cell.Add(relativeMoves[random], pos);
		pos = newMove;

		//In case newMove is on polymer, update polymer to know that the protein is stuck to it
		if (grid.getCell(newMove) != -1) {
			int index = grid.getCell(newMove);
			poly.updateOccupation(index, 1);
		}
	}

	//Update protein on monomer 0
	private void updateEndProtein(int index) {

		//Protein goes forwards along the DNA chain
		if (index == 0 && rndm.nextDouble() < AM.getProbRight(0)) {
			pos = poly.getPoly((1)).getPos();
			poly.updateOccupation(1, 1);
		}
		
		else if (index == (N-1) && rndm.nextDouble() < AM.getProbLeft(N-1)) {
			pos = poly.getPoly((N-2)).getPos();
			poly.updateOccupation(N-2, 1);
		}

		//Protein jumps off
		else {
			//Get relative map from END TO NEAREST
			Map map;
			if (index == 0) {
				map = Map.getRelativeMap(poly.getPoly(index).getPos(), poly.getPoly(index+1).getPos());
			}
			else { map = Map.getRelativeMap(poly.getPoly(index).getPos(), poly.getPoly(index-1).getPos()); }

			for (int i=0; i<6; i++) {
				if (Map.compareMaps(map, endMaps[i]) == true) {
					conf = endConfs[i]; break;	//Does break break out of if or out of for loop?
				}
			}

			/*
			 * Probability of either move in configuration is
			 * 1/5. Choose one at random. Add the relative move to the
			 * position of nearest to get the new move of the end
			 * monomer. Try the move: If cell is empty, perform move.
			 */
			Map[] relativeMoves = conf.getPossibleMoves();
			int random = (int) (rndm.nextDouble()*5);
			Cell newMove = Cell.Add(relativeMoves[random], pos);
			pos = newMove;

			//In case newMove is on polymer, update polymer to know that the protein is stuck to it
			if (grid.getCell(newMove) != -1) {
				int newIndex = grid.getCell(newMove);
				poly.updateOccupation(newIndex, 1);
			}
		}
	}

	private void updateInternalProtein(int index) {
		
		double tempRndm = rndm.nextDouble();
		
		//Go left
		if (tempRndm < AM.getProbLeft(index)) {
			pos = poly.getPoly(index-1).getPos();
			poly.updateOccupation(index-1, 1);
		}
		
		//Go right
		else if (tempRndm < AM.getProbOn(index)) {	//prob on = prob left + prob right
			pos = poly.getPoly(index+1).getPos();
			poly.updateOccupation(index+1, 1);
		}

		//Jump off polymer
		else {
			//System.out.println("Jump off at: " + index + " with aff: " +AM.getAffinity(index) + " and prob to jump off: " + AM.getProbOff(index));
			Map[] map = Map.getRelativeMap(poly.getPoly(index-1).getPos(),
					poly.getPoly(index).getPos(), poly.getPoly(index+1).getPos());

			for (int i=0; i<30 ; i++) {
				if (Map.compareMaps(map, internalMaps[i]) == true) {
					conf = internalConfs[i];  break;	//Does break break out of if or for loop?
				}
			}

			/*
			 * Probability of either move in configuration is
			 * 1/4. Choose one at random. Add the relative move to the
			 * position of nearest to get the new move of the end
			 * monomer. Try the move: If cell is empty, perform move.
			 */
			Map[] relativeMoves = conf.getPossibleMoves();
			int random = (int) (rndm.nextDouble()*4);
			Cell newMove = Cell.Add(relativeMoves[random], pos);
			pos = newMove;

			//In case newMove is on polymer, update polymer to know that the protein is stuck to it
			if (grid.getCell(newMove) != -1) {
				int newIndex = grid.getCell(newMove);
				poly.updateOccupation(newIndex, 1);
			}
		}
	}



	/*
	 * Override the following method and throw exception if called.
	 * This method is unusable for this kind of protein.
	 */
	@Override
	public void setAffinity(double newAff) {
		//Not currently possible
//		try {
//			throw new Exception("Impossible to set affinity for AffProtein using this method.");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public AffinityManager getAM() { return AM; }
}
