package cl.AffinityProtein;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cl.DNA.Polymer;
import cl.DNA.PolymerOneProtein;
import cl.Lattice.Cell;
import cl.ProteinDNASystem.SimpleSystem;

/*
 * Panel associated with PDE solver
 */
public class Panel extends JPanel implements ActionListener, ChangeListener, KeyListener{

	private static final long serialVersionUID = 1L;
	SimpleSystem s;

	// Constructor taking SimpleSystem instant
	public Panel(SimpleSystem s) {
		this.s = s;
		this.setLayout(null);
	}


	/*
	 * Paint component dictating simulation graphics
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		//Set size of panel
		// First, get the dimensions of the component
		int width = getWidth(), height = getHeight();

		g.setColor(Color.black);
		g.drawString("Protein on DNA: " + s.getProt().getOnDNA(), 50,80);
		PolymerOneProtein poly = (PolymerOneProtein) s.getPoly();
		AffProtein prot = (AffProtein) s.getProt();
		AffinityManager AM = prot.getAM();
		
		//Colour code according to affinity values
		double min = 0.0;
		double max = AM.getTargetAff();
		
		//Draw protein
		if (prot.getOnDNA() == 1) {
			int pos = poly.grid.getCell(prot.getPos());
			g.setColor(Color.red);
			drawCircle(g,9+pos*8,90,4);
		}
		
		//Draw circles
		for (int x=9, y=100, i=0; i<poly.getDNALength(); i++) {
			double value = AM.getAffinity(i);
			int red = (int) (((value-min)/(max-min))*255);
			Color color = new Color(red, 0, 255);
			g.setColor(color);
			drawCircle(g,x,y,4);
			x += 8;
		}
		
		//Draw golf course
		g.setColor(Color.BLACK);
		for (int x=5, i=0; i<poly.getDNALength(); i++) {
			double value = AM.getAffinity(i);
			for (int j=0, y=120; j < (int) value; j++) {
				g.drawRect(x, y, 8, 8);
				y += 8;
			}
			x +=8;
		}

		//        int red = (int) (((value-min)/(max-min))*255);
		//		
		//		if (value>=0) {
		//			color = new Color(red, 0, 255);
		//        
		//			}
	}

	/*
	 * Method for drawing a circle
	 */
	private void drawCircle(Graphics g, int x, int y, double r) {

		g.fillOval((int)Math.round(x-r) , (int)Math.round(y- r), (int)(2.0*r), (int)(2.0*r));
	}


	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
}
