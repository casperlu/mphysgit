package cl.AffinityProtein;

import java.util.Random;
import cl.DNA.ExtendedGrid;
import cl.DNA.PolymerManyAffProteins;
import cl.Lattice.Cell;

public class ManyAffProteins {

	//Internal variables
	private AffProteins[] proteinArray;
	private ExtendedGrid protGrid;
	private int N;

	public ManyAffProteins(int numOfProteins, int boxSize, PolymerManyAffProteins p, Random r, double target, AffinityManager affMan) {

		N = numOfProteins;
		protGrid = new ExtendedGrid(boxSize);

		if (N!= 0) {
			proteinArray = new AffProteins[N];
			for (int ID=0; ID<N; ID++) {
				//Do while loop makes sure two proteins aren't placed on top of each other
				do {
					proteinArray[ID] = new AffProteins(boxSize, p, r, target, affMan);	//place at random in box
				} while(protGrid.getCell(proteinArray[ID].getPos()) != -1);
				proteinArray[ID].setID(ID);
				proteinArray[ID].setGrid(protGrid);
				protGrid.update(proteinArray[ID].getPos(), ID);	//Add protein to grid
			}
		}
	}

	public void update() {
		for (int i=0; i<N; i++) {
			proteinArray[i].update();
		}
	}

	public AffProtein getProt(int index) { return proteinArray[index]; }
	public int getProtGrid(Cell c) { return protGrid.getCell(c); }
	public void updateProtGrid(Cell position, int newValue) { protGrid.update(position, newValue); }
	public void doMove(int index, Cell newMove) { proteinArray[index].doMove(newMove); }

	public boolean getTargetStatus() {
		for (int i=0; i<N; i++) {
			if (proteinArray[i].getTargetStatus() == true) {
				return true;
			}
		}
		return false;
	}

	public String toVMD() {
		String output = "";
		for (int i=0; i<N; i++) {
			output += "Protein" + i + " " + proteinArray[i].getPos().toString() + "\n";
		}
		return output;
	}
}
