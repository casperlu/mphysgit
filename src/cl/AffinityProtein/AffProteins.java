package cl.AffinityProtein;

import java.util.Random;

import cl.Configurations.EndConfiguration;
import cl.DNA.ExtendedGrid;
import cl.DNA.PolymerOneProtein;
import cl.Lattice.Cell;
import cl.Lattice.Map;


public class AffProteins extends AffProtein {

	//Internal variables
	protected ExtendedGrid protGrid = null;//Must be initialised somewhere
	protected int ID = -1; //MUST BE INITIALISED ELSEWHERE!


	//Produce protein at random position
	public AffProteins(int boxSize, PolymerOneProtein p, Random r, 
			double target, AffinityManager affMan) {
		//Give Protein class NaN for the affinity (cheap way of not having to copy loads of code)
		super(boxSize, p, r, target, affMan);
	}

	//Produce protein at specific position
	public AffProteins(int boxSize, Cell position, PolymerOneProtein p,
			Random r, double target, AffinityManager affMan) {
		//Give Protein class NaN for the affinity (cheap way of not having to copy loads of code)
		super(boxSize, position, p, r, target, affMan);
	}

	/*
	 * Important setters. Must be invoked before class can run correctly.
	 */
	public void setGrid(ExtendedGrid pGrid) { protGrid = pGrid; }
	public void setID(int id) { ID = id; }

	//Override update method to check for more proteins before move
	@Override
	public void update() {

		//3D diffusion
		if (grid.getCell(pos) == -1) {
			update3dDiffusion();
		}

		//Possible 1D diffusion
		else {

			//The protein is now on the DNA
			onDNA = 1;

			//Get the index of the monomer it is attached to
			int index = grid.getCell(pos);

			//Check if monomer is the target
			if (poly.getTarget(index) == target) { targetFound = true; }


			//Only if protein is not at target, should it continue searching
			if (targetFound == false) {

				//Notify polymer that protein is no longer on monomer[index]
				poly.updateOccupation(index, -1);

				//If protein is attached to an end monomer
				if (index == 0 || index == (N-1)) {
					updateEndProtein(index);
				}

				//If protein is attached to an internal monomer
				else {	updateInternalProtein(index); }
			}
		}
	}

	private void update3dDiffusion() {
		//Do 3d random walk
		onDNA = 0;
		conf = EndConfiguration.ENDMONOMER;		//Protein is same as for endmonomer
		Map[] relativeMoves = conf.getPossibleMoves();

		/*
		 * Probability of either move in configuration is
		 * 1/6. Choose one at random. Add the relative move to the
		 * position of nearest to get the new move of the end
		 * monomer. Try the move: If cell is empty, perform move.
		 */
		int random = (int) (rndm.nextDouble()*6);
		Cell newMove = Cell.Add(relativeMoves[random], pos);

		//Only make move if cell is not occupied by another protein
		if (protGrid.getCell(newMove) == -1) {
			doMove(newMove);

			//In case newMove is on polymer, update polymer to know that the protein is stuck to it
			if (grid.getCell(newMove) != -1) {
				int index = grid.getCell(newMove);
				poly.updateOccupation(index, 1);
			}
		}
	}

	//Update protein on monomer 0
	private void updateEndProtein(int index) {

		//Protein goes forwards along the DNA chain
		if (index == 0 && rndm.nextDouble() < AM.getProbRight(0)) {

			//Only make move if cell is not occupied by another protein
			if (protGrid.getCell(poly.getPoly(1).getPos()) == -1) {

				doMove(poly.getPoly((1)).getPos());
				poly.updateOccupation(1, ID);
			}

			//Else protein doesn't move
			else {
				poly.updateOccupation(index, ID);	//Tell poly protein hasn't moved
			}
		}

		//Protein goes backwards along the DNA chain
		else if (index == (N-1) && rndm.nextDouble() < AM.getProbLeft(N-1)) {

			//Only make move if cell is not occupied by another protein
			if (protGrid.getCell(poly.getPoly((index-1)).getPos()) == -1) {

				doMove(poly.getPoly((N-2)).getPos());
				poly.updateOccupation(N-2, ID);
			}

			//Else protein doesn't move
			else {
				poly.updateOccupation(index, ID);
			}
		}

		//Protein jumps off
		else {
			//Get relative map from END TO NEAREST
			Map map;
			if (index == 0) {
				map = Map.getRelativeMap(poly.getPoly(index).getPos(), poly.getPoly(index+1).getPos());
			}
			else { map = Map.getRelativeMap(poly.getPoly(index).getPos(), poly.getPoly(index-1).getPos()); }

			for (int i=0; i<6; i++) {
				if (Map.compareMaps(map, endMaps[i]) == true) {
					conf = endConfs[i]; break;	//Does break break out of if or out of for loop?
				}
			}

			/*
			 * Probability of either move in configuration is
			 * 1/5. Choose one at random. Add the relative move to the
			 * position of nearest to get the new move of the end
			 * monomer. Try the move: If cell is empty, perform move.
			 */
			Map[] relativeMoves = conf.getPossibleMoves();
			int random = (int) (rndm.nextDouble()*5);
			Cell newMove = Cell.Add(relativeMoves[random], pos);



			//Only make move if cell is not occupied by another protein
			if (protGrid.getCell(newMove) == -1) {

				doMove(newMove);

				//In case newMove is on polymer, update polymer to know that the protein is stuck to it
				if (grid.getCell(newMove) != -1) {
					int newIndex = grid.getCell(newMove);
					poly.updateOccupation(newIndex, ID);
				}
			}

			//Else protein doesn't move
			else { poly.updateOccupation(index, ID); }
		}
	}

	private void updateInternalProtein(int index) {

		double tempRndm = rndm.nextDouble();

		//Go left
		if (tempRndm < AM.getProbLeft(index)) {

			//Only make move if cell is not occupied by another protein
			if (protGrid.getCell(poly.getPoly((index-1)).getPos()) == -1) {

				doMove(poly.getPoly(index-1).getPos());
				poly.updateOccupation(index-1, ID);
			}

			//Else protein doesn't move
			else {
				poly.updateOccupation(index, ID);
			}
		}

		//Go right
		else if (tempRndm < AM.getProbOn(index)) {	//prob on = prob left + prob right

			//Only make move if cell is not occupied by another protein
			if (protGrid.getCell(poly.getPoly((index+1)).getPos()) == -1) {

				doMove(poly.getPoly(index+1).getPos());
				poly.updateOccupation(index+1, ID);
			}

			//Else protein doesn't move
			else {
				poly.updateOccupation(index, ID);
			}
		}

		//Jump off polymer
		else {
			//System.out.println("Jump off at: " + index + " with aff: " +AM.getAffinity(index) + " and prob to jump off: " + AM.getProbOff(index));
			Map[] map = Map.getRelativeMap(poly.getPoly(index-1).getPos(),
					poly.getPoly(index).getPos(), poly.getPoly(index+1).getPos());

			for (int i=0; i<30 ; i++) {
				if (Map.compareMaps(map, internalMaps[i]) == true) {
					conf = internalConfs[i];  break;	//Does break break out of if or for loop?
				}
			}

			/*
			 * Probability of either move in configuration is
			 * 1/4. Choose one at random. Add the relative move to the
			 * position of nearest to get the new move of the end
			 * monomer. Try the move: If cell is empty, perform move.
			 */
			Map[] relativeMoves = conf.getPossibleMoves();
			int random = (int) (rndm.nextDouble()*4);
			Cell newMove = Cell.Add(relativeMoves[random], pos);

			//Only make move if cell is not occupied by another protein
			if (protGrid.getCell(newMove) == -1) {

				doMove(newMove);

				//In case newMove is on polymer, update polymer to know that the protein is stuck to it
				if (grid.getCell(newMove) != -1) {
					int newIndex = grid.getCell(newMove);
					poly.updateOccupation(newIndex, ID);
				}
			}

			//Else protein doesn't move
			else { poly.updateOccupation(index, ID); }
		}
	}

	/*
	 * Private method for performing a move:
	 *  - removes protein from protGrid
	 *  - changes protein position to newMove
	 *  - adds protein to protGrid again
	 */
	public void doMove(Cell newMove) {
		protGrid.update(pos, -1); //Remove protein from grid
		pos = newMove;
		protGrid.update(pos, ID); //Add protein to grid again
	}
}