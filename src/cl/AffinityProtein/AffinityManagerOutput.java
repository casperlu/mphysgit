package cl.AffinityProtein;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class AffinityManagerOutput {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		//Define the protein for the affinity manager
		long AMSeed = 5;
		double stDev = 3;
		double mean = 0.0;
		double cutOffAff = 20; 
		double targetAff = 50.16;
		double DNAAff = 4.0;
		AffinityManager AM = new AffinityManager(1000, DNAAff, mean, stDev, 500, targetAff, cutOffAff, AMSeed);
		double[] affs = AM.getAffinities();
		
		PrintWriter printer = new PrintWriter(new FileWriter("AffinityManagerOutputDNAAff4.dat"));
		
		for (int i=0; i<affs.length; i++) {
			printer.println(i + "\t" + affs[i]);
		}
		System.out.println("done.");
		printer.close();
	}

}
