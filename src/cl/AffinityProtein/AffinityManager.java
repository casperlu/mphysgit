package cl.AffinityProtein;

import cl.Random.BMgenerator;

public class AffinityManager {

	//TODO: A lot of optimisation is possible here. Do this once you are happy with structure.

	//Internal variables
	private int N;
	private double[] affinities;
	private double[] probLeft, probRight, probOff;
	private double targetAff;


	/*
	 * Affinitiy manager for generating affinities and probabilities for statistical properties
	 * e.g. mean, stDev etc.
	 */
	public AffinityManager(int N, double DNAAff, double mean, double stDev, 
			int target, double targetAff, double cutOffAff, long seed) {
		this.N = N;;
		this.targetAff = targetAff;

		//Generate the affinities from a gaussian distribution
		affinities = BMgenerator.generateGaussian(N, mean, stDev, cutOffAff, seed);

		//Set all affinities to be max(affinities generated by gaussian, DNAAff)
		for (int i=0; i<N; i+=2) {
			affinities[i] = max(affinities[i], DNAAff);
			affinities[i+1] = affinities[i];	//Set next monomer as same affinity

			/* Ideally also take into account of N not even. */
		}
		affinities[target] = targetAff;	//Correct the target site to have a specific affinity

		//Generate the probabilities for jumping along/off DNA chain
		generateProbabilities();
	}

	/*
	 * Constructor for providing a specific arrray of affinities.
	 * Computes probabilities
	 */
	public AffinityManager(double DNAAff, int target, double[] affs) {
		affinities = affs;
		N = affinities.length;
		this.targetAff = affinities[target];

		//Set all affinities to be max(affinities, DNAAff)
		for (int i=0; i<N; i+=2) {
			
			affinities[i] = max(affinities[i], affinities[i+1], DNAAff);
			affinities[i+1] = affinities[i];	//Set next monomer as same affinity

			/* Ideally also take into account of N not even. */
		}

		//Generate the probabilities for jumping along/off DNA chain
		generateProbabilities();
	}

	private void generateProbabilities() {

		//Probability of jumping to the left (left implies decrease)
		probLeft = new double[N];
		//Probability of jumping to the right (right implies increase)
		probRight = new double[N];
		//Probability of jumping off the DNA
		probOff = new double[N];

		//Do leftmost monomer first
		probLeft[0] = Double.NaN;	//Can't jump any more to the left
		/*
		 * Notice no minus sign in first exponential. This is due to the convention of
		 * affinities being positive, e.g. affinities[0] = 4.5 actually means = -4.5
		 * This is a tad confusing. Maybe redo entire program?
		 */
		double totalProb = (1.0/6.0)*min(Math.exp((affinities[1]-affinities[0])), 1.0)
				+ (5.0/6.0)*Math.exp(-affinities[0]);
		probRight[0]= (1.0/6.0)*min(Math.exp((affinities[1]-affinities[0])), 1.0)/totalProb;
		probOff[0] = 1-probRight[0];


		//Do all internal monomers
		for (int i=1; i<N-1; i++) {
			totalProb = (1.0/6.0)*min(Math.exp((affinities[i-1]-affinities[i])), 1.0)
					+ (2.0/3.0)*Math.exp(-affinities[i])
					+ (1.0/6.0)*min(Math.exp((affinities[i+1]-affinities[i])), 1.0);
			probLeft[i] = (1.0/6.0)*min(Math.exp((affinities[i-1]-affinities[i])), 1.0)/totalProb;
			probRight[i] = (1.0/6.0)*min(Math.exp((affinities[i+1]-affinities[i])), 1.0)/totalProb;
			probOff[i] = 1-probLeft[i]-probRight[i];
			//System.out.println(i + " " + affinities[i] + " " + probLeft[i] + " " + probRight[i] + " " + probOff[i]);
		}

		//Do last monomer
		totalProb = (1.0/6.0)*min(Math.exp((affinities[N-2]-affinities[N-1])), 1.0)
				+ (5.0/6.0)*Math.exp(-affinities[N-1]);
		probLeft[N-1]= (1.0/6.0)*min(Math.exp((affinities[N-2]-affinities[N-1])), 1.0)/totalProb;
		probRight[N-1] = Double.NaN;	//Can't jump any further to the right
		probOff[N-1] = 1- probLeft[N-1];
	}

	/*
	 * Getters
	 */
	public double getProbLeft(int i) { return probLeft[i]; }
	public double getProbRight(int i) { return probRight[i]; }
	public double getProbOff(int i) { return probOff[i]; }
	public double getProbOn(int i) { return 1.0-probOff[i]; }
	public double getTargetAff() { return targetAff; }
	public double getAffinity(int i) { return affinities[i]; }
	public double[] getAffinities() { return affinities; }


	/*
	 *	Find minimum of two doubles 
	 */
	private double min(double a, double b) {
		return a<b? a: b;
	}

	/*
	 *	Find maximum of two doubles 
	 */
	private double max(double a, double b) {
		return a>b? a: b;
	}
	
	private double max(double a, double b, double c) {
		double maxOfAB = max(a,b);
		return max(maxOfAB, c);
	}
}
