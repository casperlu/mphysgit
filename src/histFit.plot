mean = 3.1732636490036485
stDev = 2.691639844595965
gaussian(x) = A*exp(-(x-mean)*(x-mean)/(2*stDev*stDev)) + B
fit gaussian(x) "./histHighestPWMAff.out" u 1:2 via A,B
plot "./histHighestPWMAff.out" u 1:2 with boxes
replot gaussian(x)
