set title "Search time versus affinity for DNA of length 1000"
set xlabel "Affinity"
set ylabel "Search time in MC sweeps"
set key left top
plot "Dist200DNA1000Seeds500.dat" u 1:2 w l
replot "Dist200Seeds500.dat" u 1:2 w l
