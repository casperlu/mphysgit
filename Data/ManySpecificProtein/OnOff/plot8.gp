set xlabel "Affinity"
set ylabel "Probability of target site being on when reached by polymerase"
set yrange[0:1]
plot "RandomDNA1000PolyAff8.0Seeds500.dat" u 1:5 w l lw 3
replot "Dist25DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
replot "Dist50DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
replot "Dist100DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
replot "Dist200DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
replot "Dist300DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
replot "Dist400DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
replot "Dist500DNA1000PolyAff8.0Seeds500.dat" u 1:5 w l
