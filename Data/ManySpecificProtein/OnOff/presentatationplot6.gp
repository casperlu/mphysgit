set title "Transcription factor blocking on DNA of length 1000 and polymerase with affinity 6"
set xlabel "Affinity"
set ylabel "Probability of target site being free when reached by polymerase"
set yrange[0:1]
set key left bottom
plot "RandomDNA1000PolyAff6.0Seeds500.dat" u 1:5 w l lw 3 title "TF random"
replot "Dist25DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 25"
replot "Dist50DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 50"
replot "Dist100DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 100"
replot "Dist200DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 200"
replot "Dist300DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 300"
replot "Dist400DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 400"
replot "Dist500DNA1000PolyAff6.0Seeds500.dat" u 1:5 w l title "TF at 500"
