set xlabel "Affinity"
set ylabel "Search time"
plot "../normals/DNA1000Seeds500.dat" u 1:($2)/10 w l
replot "Prot10DNA1000Seeds200.dat" u 1:2 w l
