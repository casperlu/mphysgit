set xlabel "Affinity"
set ylabel "Search time"
plot "../normals/DNA200Seeds500.dat" u 1:($2)/10 w l
replot "Prot10DNA200Seeds200.dat" u 1:2 w l
