totalProb(x) = (1.0/3.0) + (2.0/3.0)*exp(-x)
probStayOn(x) = (1.0/3.0)/(totalProb(x))
ls(x) = sqrt(1.0/(abs(log(probStayOn(x)))))
V = 29791
L = 300
D = 0.5
fit(x) = A*V/(D*ls(x)) + B*L*ls(x)/D
fit [1:10] fit(x) "500SeedsDNA300.dat" u 1:2 via A,B
plot "500SeedsDNA300.dat" u 1:2 w lp pt 7 ps 2, fit(x)
