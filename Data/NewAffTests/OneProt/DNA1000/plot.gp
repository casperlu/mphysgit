totalProb(x) = (1.0/3.0) + (2.0/3.0)*exp(-x)
probStayOn(x) = (1.0/3.0)/(totalProb(x))
ls(x) = sqrt(1.0/(abs(log(probStayOn(x)))))
V = 97336
L = 1000
D = 0.5
fit(x) = A*V/(D*ls(x)) + B*L*ls(x)/D
fit [1.3:10.8] fit(x) "DNA1000Seeds500.dat" u 1:2 via A,B
plot "DNA1000Seeds500.dat" u 1:2:4 with errorbars, fit(x)
