totalProb(x) = (1.0/3.0) + (2.0/3.0)*exp(-x)
probStayOn(x) = (1.0/3.0)/(totalProb(x))
ls(x) = sqrt(1.0/(abs(log(probStayOn(x)))))
V = 19683
L = 200
D = 0.5
fit(x) = A*V/(D*ls(x)) + B*L*ls(x)/D
fit fit(x) "DNA200Seeds500.dat" u 1:2 via A,B
plot "DNA200Seeds500.dat" u 1:2:4 with errorbars, fit(x)
