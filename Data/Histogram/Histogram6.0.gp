plot "Hist6.0Log.dat" u 1:2
#logAdd = 1/1000	#Need to add this constant to x, as we added 1 to all histogram values in order to avoid log(0) issues
#fit(x) = -A*((x+logAdd)**B)+C
#fit fit(x) "Hist6.0Log.dat" u 1:2 via A,B,C
fit(x) = -0.000055*x + 4.8
plot "Hist6.0Log.dat" u 1:2 smooth freq with boxes, fit(x)